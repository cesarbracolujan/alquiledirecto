## Alquile Directo App

La siguiente aplicación se puede desplegar localmente utilizando contenedores, solo se necesita tener instalado Docker para poder ejecutar. A continuación se nuestra los comandos para poder compilar la aplicación y sus dependencias, asi como ejecutar localmente.

- Requisitos:
    Docker Engine.


#### Compilación de la aplicación

Primero generamos el .env con las configuraciones de conexión y demás parametros para que la aplicacion funciones, este archivo debe crearse dentro de la carpeta src.

Dentro de la carpeta raíz, ejecutamos el siguiente comando para crear el contenedor que permitirá realizar las compilaciones tanto de laravel como de vue.
`docker-compose build app`

Luego de tener el contenedor construido, realizamos el siguiente comando para ejecutar los contenedores que alojan la aplicación y la base de datos.
`docker-compose up -d`

Teniendo los contenedores ejecutando, podemos verificar con el comando `docker-compose ps` y nos devolverá 3 contenedores ejecutandose:
- laravel-app: Contenedor para compilar laravel
- laravel-db: Contenedor que contiene la instancia Mysql para la BD
- laravel-nginx: Contenedor para ejecutar la aplicación Alquiledirecto

Luego de esto, tenemos que restaurar las dependencias tanto de laravel como del vue, esto lo realizamos con los siguientes comandos:

`docker-compose exec app composer install`
`docker-compose exec app php artisan config:cache`
`docker-compose exec app php artisan migrate:refresh --seed`
`docker-compose exec app yarn install`
`docker-compose exec app yarn production`

Con estos comandos realizamos la compilación inicial para la aplicación.

#### Realizar implementaciones

Para realizar cambios en la aplicación y tener la aplicación en modo desarrollo, tener en cuenta estos comandos para replicar los cambios:

`docker-compose exec app composer install` - Para agregar modulos a laravel
`docker-compose exec app php artisan config:cache` - Para reflejar las configuraciones del archivo .env
`docker-compose exec app php artisan migrate:refresh --seed` - Para aplicar una nueva migracion a la BD
`docker-compose exec app yarn install` - Para instalar un módulo de Nodejs
`docker-compose exec app yarn watch` - Para mantener el codigo compilando en modo escucha de la parte fronend que está en Vue.