<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/politicas', function () {
    return view('about/privacy_polices');
});

Route::get('/terminos', function () {
    return view('about/terms_conditions');
});

Route::get('/nosotros', function () {
    return view('about/about_us');
});

Route::get('/adquiriente', function () {
    return view('about/for_advertiser');
});

Route::get('/anunciante', function () {
    return view('about/for_acquirer');
});

Route::get('/preguntas-frecuentes', function () {
    return view('about/faqs');
});

Route::prefix('home')->group(function () {
    Route::get('tables', 'HomeController@tables');
    Route::get('remoteSearch', 'HomeController@remoteSearch');
});



Route::prefix('ads')->group(function () {
    Route::get('', 'AdsController@index')->name('ads.index');
    Route::get('create', 'AdsController@create')->name('ads.create')->middleware(['auth']);
    Route::get('tables', 'AdsController@tables');
    Route::get('departments', 'AdsController@getDepartments');
    Route::get('provinces/{id}', 'AdsController@getProvinceByDepartmentId');
    Route::get('districts/{id}', 'AdsController@getDistrictByProvinceId');
    Route::post('records', 'AdsController@records');
    Route::get('record/{record}', 'AdsController@record');
    Route::get('show/{record}', 'AdsController@show')->name('ads.show');
    Route::get('remoteSearch', 'AdsController@remoteSearch');
    Route::post('upload', 'AdsController@upload');
    Route::post('', 'AdsController@store');
    Route::post('favorite/{post}', 'AdsController@favoritePost');
    Route::post('unfavorite/{post}', 'AdsController@unFavoritePost');
    Route::get('features/{id}','AdsController@getFeaturesByTypeOfProperty');
    Route::post('save', 'AdsController@saveAd');
    Route::get('fechas/{id}','AdsController@getFechas');
});

Route::prefix('profile')->group(function (){
    Route::get('edit','UserController@index')->name('profile.edit')->middleware('verified');
    Route::post('update','UserController@update')->name('profile.update');
    Route::get('favorite','UserController@favorite')->name('profile.favorite')->middleware('auth');
    Route::post('favorities/{id}','UserController@getFavorities');
    Route::get('myads','UserController@ourAds')->name('profile.myads')->middleware('auth');
    Route::post('myads/{id}','UserController@getOurAds');
    Route::post('fechas/{id}','AdsController@setFechas');
});

Route::prefix('notifications')->group(function (){
    Route::post('question','NotificationController@sendQuestionToAuthor');
});

Route::prefix('payment')->group(function () {
    Route::get('', 'PaymentController@index')->name('payment.index');
});


Auth::routes(['verify' => true,'reset' => true]);

Route::get('/home', 'HomeController@index')->name('home');


Route::get('login/{driver}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{driver}/callback', 'Auth\LoginController@handleProviderCallback');
