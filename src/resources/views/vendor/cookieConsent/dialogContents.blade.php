<div class="cookie-bar js-cookie-consent cookie-consent">

    <p class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </p>

    <button class="btn btn-solid btn-xs js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>
    <a href="javascript:void(0)" class="btn btn-solid btn-xs">Rechazar</a>
</div>
