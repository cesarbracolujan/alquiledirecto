<p>Hola {{ $question->username }}</p>
<div>Tienes una pregunta realizada al anuncio <a href="{{ $question->url }}">{{ $question->post }}</a></div>
<div>&nbsp;</div>
<div>Datos de Contacto:</div>
<div>
<ul>
<li>Nombre: {{ $question->contact_name }}"</li>
<li>Email: <a href="mailto:{{ $question->contact_email }}" target="_blank" rel="noopener">{{ $question->contact_email }}</a></li>
<li>Tel&eacute;fono: <a href="https://wa.me/{{ $question->contact_phone }}">{{ $question->contact_phone }}</li>
</ul>
<div>Mensaje:&nbsp;</div>
</div>
<div>
<table style="height: 57px;" width="357">
<tbody>
<tr>
<td style="width: 347px;">
<blockquote><em><span style="color: #0000ff;">"{{ $question->message }}"</span></em></blockquote>
</td>
</tr>
</tbody>
</table>
</div>
<div>&nbsp;</div>
<div>Por favor s&iacute;rvase contactar con el usuario por los datos de contactos proporcionados.</div>
<div>&nbsp;</div>
<div>Saludos</div>
<div>&nbsp;</div>
<div><img src="https://www.alquiledirecto.com/images/logos/logo-alquiledirecto.png" alt="firma" width="188" height="34" /></div>
