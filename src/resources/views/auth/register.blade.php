@extends('layouts.app')

@section('content')
<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>Crear Cuenta</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Crear Cuenta</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->

<section class="register-page section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Nuevo Usuario</h3>
                <div class="theme-card">
                    <form class="theme-form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-row row">
                            <div class="col-md-6 mb-3">
                                <label for="firstname">Nombres</label>
                                <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" id="firstname" value="{{ old('firstname') }}" placeholder="Ingresar Nombres" autofocus>
                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastname">Apellidos</label>
                                <input type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" id="lastname" placeholder="Ingresar Apellidos"
                                    required autofocus>
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row row">
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Ingresar Email" required autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="cellphone">Celular</label>
                                <input type="text" class="form-control @error('cellphone') is-invalid @enderror" name="cellphone" id="cellphone" value="{{ old('cellphone') }}" placeholder="000 000 000" required autofocus>
                                @error('cellphone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div>
                                    <input type="checkbox" name="has_whatsapp" id="has_whatsapp" value="1">
                                    <label for="has_whatsapp">¿Tu celular utiliza Whatsapp?</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row row">
                            <div class="col-md-6">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Ingresar Contraseña" required autofocus>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="password-confirm">Confirmar Contraseña</label>
                                <input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Confirmar tu contraseña" required="">
                                @error('password-confirm')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" class="@error('terms_accepted') is-invalid @enderror" name="terms_accepted" id="terms_accepted" value="1" required>
                                @error('terms_accepted')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="terms_accepted">Al registrarme estoy de acuerdo con los <a href="/terminos" target="_blank">Términos y Condiciones</a> y <a href="/politicas" target="_blank" >Políticas de Privacidad </a> de Alquile Directo</label>

                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-solid">Crear Cuenta</button>
                                <hr/>
                                <div class="">¿Tienes una cuenta? <a href="{{ route('login')}}"> Inicia Sesión</a></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
