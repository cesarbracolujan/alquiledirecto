@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <section class="login-page section-b-space">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h3>Login</h3>
                        @if(session('info'))
                            <div class="alert alert-danger">
                                {{ session('info') }}
                            </div>
                        @endif

                        <div class="theme-card">
                            <form class="theme-form" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Ingresar email" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Ingresar password" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <a href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
                                </div>
                                <div class="typo-content typo-buttons">
                                    <button type="submit" class="btn btn-solid p-right col-12 col-md-5">
                                        <i class="fa fa-sign-in"></i>&ThinSpace;Ingresar
                                    </button>
                                </div>

                                <div class="form-row row justify-content-md-center">
                                    <p class="mt-3 separator">o Iniciar sesión con</p>
                                    <div class="col-md-5 col-12 align-self-center">
                                        <a href="{{ url('login/facebook')}}" class="btn btn-outline col-12"><i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;Facebook</a>
                                    </div>
                                    <div class="col-md-5 col-12 align-self-center">
                                        <a href="{{ url('login/google')}}" class="btn btn-outline col-12"><i class="fa fa-google"></i>&nbsp;&nbsp;&nbsp;Google</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 right-login">
                        <h3>Nuevo Usuario</h3>
                        <div class="theme-card authentication-right">
                            <h6 class="title-font">Crear un nuevo usuario</h6>
                            <p>Registrar una cuenta libre, te permitirá navegar por todos los anuncios con la completa información para alquilar. Además te permitirá publicar anuncios hacia el público</p>
                            <a href="{{ url('register')}}" class="btn btn-solid"><i class="fa fa-user"></i>&ThinSpace;Crear Usuario</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
