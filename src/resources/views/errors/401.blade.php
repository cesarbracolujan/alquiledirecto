@extends('layouts.app')

@section('content')
<!-- section start -->
<section class="p-0">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="error-section">
                    <h1>401</h1>
                    <h2>Sección no autorizada.</h2>
                    <a href="{{ route('home') }}" class="btn btn-solid">Regresar</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->

@endsection
