@extends('layouts.app')

@section('content')
<!-- section start -->
<section class="p-0">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="error-section">
                    <h1>500</h1>
                    <h2>Error interno del servidor.</h2>
                    <a href="{{ route('home') }}" class="btn btn-solid">Regresar</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->

@endsection
