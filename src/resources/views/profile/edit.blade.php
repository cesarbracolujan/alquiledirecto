@extends('layouts.app')

@section('content')

<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>Perfil</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">perfil</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->

<!-- personal deatail section start -->
<section class="contact-page register-page section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        {{ $message }}
                    </div>
                    @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning" role="alert">
                        {{ $message }}
                    </div>
                @endif
                <form class="theme-form" method="POST" action="{{ route('profile.update') }}">
                    @csrf
                    <h3>Información Personal</h3>
                    <div class="form-row row">
                        <div class="col-md-6">
                            <label for="firstname">Nombres</label>
                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Ingresa tus nombres"
                                required="" value="{{ $currentUser->firstname }}" >
                        </div>
                        <div class="col-md-6">
                            <label for="lastname">Apellidos</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Ingresa tus apellidos"
                                required="" value="{{ $currentUser->lastname }}">
                        </div>
                        <div class="col-md-6">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{ $currentUser->email }}" disabled>
                        </div>
                        <div class="col-md-6">
                            <label for="phone">Celular</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $currentUser->phone }}" placeholder="Ingrese su número celular"
                                required="">
                        </div>
                        <div class="col-md-6">
                            <label for="contac_name">Razón Social</label>
                            <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Ingresa Razón Social"
                                    value="{{ $currentUser->contact_name }}">
                        </div>
                        <div class="col-md-6">
                            <label for="address">Dirección</label>
                            <input type="text" class="form-control" id="address" name="address" placeholder="Ingresa dirección de contacto"
                                required="" value="{{ $currentUser->address }}">
                        </div>
                    </div>
                    <h3>Social Media</h3>
                    <div class="form-row row">
                        <div class="col-md-12">
                            <label for="website" class="text-success"><i class="fa fa-globe"></i> Website</label>
                            <input type="text" class="form-control" id="website" name="website" placeholder="Ingresar URL de tu website"
                                value="{{ $currentUser->website }}" >
                        </div>
                        <div class="col-md-12">
                            <label for="facebook" class="text-primary"><i class="fa fa-facebook"></i> Facebook</label>
                            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Ingresar URL de tu facebook"
                                value="{{ $currentUser->facebook }}" >
                        </div>
                        <div class="col-md-12">
                            <label for="twitter" class="text-info"><i class="fa fa-twitter"></i> Twitter</label>
                            <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Ingresar URL de tu twitter"
                                value="{{ $currentUser->twitter }}" >
                        </div>
                        <div class="col-md-12">
                            <label for="youtube" class="text-danger"><i class="fa fa-youtube"></i> Youtube</label>
                            <input type="text" class="form-control" id="youtube" name="youtube" placeholder="Ingresar URL de tu youtube"
                                value="{{ $currentUser->youtube }}" >
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-solid" type="submit">Guardar</button>
                            <a href="{{ route('home') }}" class="btn btn-sm btn-outline"> Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->

@endsection
