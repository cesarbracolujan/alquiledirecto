@extends('layouts.app')

@section('content')
    @include('layouts.partials.home_slider')
    @include('layouts.partials.paragraph')
    @include('layouts.partials.product_slider')
@endsection
