{{-- <p>Hola {{ $url }}</p> --}}
<div class="">
    <div class="aHl"></div>
    <div id=":qj" tabindex="-1"></div>
    <div id=":q8" class="ii gt">
        <div id=":q7" class="a3s aiL template">
            <u></u>
            <div style="background-color:#ffffff;color:#718096;height:100%;line-height:1.4;margin:0;padding:0;width:100%!important">
                <table width="100%" cellpadding="0" cellspacing="0" role="presentation"
                    style="background-color:#edf2f7;margin:0;padding:0;width:100%">
                    <tbody>
                        <tr>
                            <td align="center"
                                style="">
                                <table width="100%" cellpadding="0" cellspacing="0" role="presentation"
                                    style="margin:0;padding:0;width:100%">
                                    <tbody>
                                        <tr>
                                            <td
                                                style="padding:25px 0;text-align:center">
                                                <img src="https://www.alquiledirecto.com/images/logos/logo-alquiledirecto.png" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" cellpadding="0" cellspacing="0"
                                                style="background-color:#edf2f7;border-bottom:1px solid #edf2f7;border-top:1px solid #edf2f7;margin:0;padding:0;width:100%">
                                                <table align="center" width="570" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="background-color:#ffffff;border-color:#e8e5ef;border-radius:2px;border-width:1px;margin:0 auto;padding:0;width:570px">

                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="max-width:100vw;padding:32px">
                                                                <span class="im">
                                                                    <h1
                                                                        style="color:#3d4852;font-size:18px;font-weight:bold;margin-top:0;text-align:left">
                                                                        Hola!</h1>
                                                                    <p
                                                                        style="font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                                                                        Por favor clic en el botón para verificar tu
                                                                        dirección de correo.</p>
                                                                    <table align="center" width="100%" cellpadding="0"
                                                                        cellspacing="0" role="presentation"
                                                                        style="margin:30px auto;padding:0;text-align:center;width:100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center"
                                                                                    style="">
                                                                                    <table width="100%" border="0"
                                                                                        cellpadding="0" cellspacing="0"
                                                                                        role="presentation"
                                                                                        style="">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td align="center"
                                                                                                    style="">
                                                                                                    <table border="0"
                                                                                                        cellpadding="0"
                                                                                                        cellspacing="0"
                                                                                                        role="presentation"
                                                                                                        style="">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td
                                                                                                                    style="">
                                                                                                                    <a href="{{ $url }}"
                                                                                                                        rel="noopener"
                                                                                                                        style="border-radius:4px;color:#fff;display:inline-block;overflow:hidden;text-decoration:none;background-color:#2d3748;border-bottom:8px solid #2d3748;border-left:18px solid #2d3748;border-right:18px solid #2d3748;border-top:8px solid #2d3748"
                                                                                                                        target="_blank">Verificación de correo electrónico</a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <p
                                                                        style="font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                                                                        Si no creó una cuenta, no es necesario realizar
                                                                        ninguna otra acción.</p>
                                                                    <p
                                                                        style="font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                                                                        Saludos cordiales,<br>
                                                                        AlquileDirecto</p>


                                                                </span>
                                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                                    role="presentation"
                                                                    style="border-top:1px solid #e8e5ef;margin-top:25px;padding-top:25px">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="">
                                                                                <p
                                                                                    style="line-height:1.5em;margin-top:0;text-align:left;font-size:14px">
                                                                                    Si tiene problemas para hacer clic
                                                                                    en el botón "Verificar dirección de
                                                                                    correo electrónico", copie y pegue
                                                                                    la URL a continuación
                                                                                    en tu navegador web: <span
                                                                                        style="word-break:break-all">
                                                                                        <a
                                                                                            href="{{ $url }}"
                                                                                            style="color:#3869d4"
                                                                                            target="_blank">{{ $url }}</a></span>
                                                                                </p>

                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td
                                                style="">
                                                <table align="center" width="570" cellpadding="0" cellspacing="0"
                                                    role="presentation"
                                                    style="margin:0 auto;padding:0;text-align:center;width:570px">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center"
                                                                style="max-width:100vw;padding:32px">
                                                                <p
                                                                    style="line-height:1.5em;margin-top:0;color:#b0adc5;font-size:12px;text-align:center">
                                                                    © 2021 AlquileDirecto. All rights reserved.</p>

                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="yj6qo"></div>
                <div class="adL">
                </div>
            </div>
            <div class="adL">

            </div>
        </div>
    </div>
    <div id=":qn" class="ii gt" style="display:none">
        <div id=":qo" class="a3s aiL "></div>
    </div>
    <div class="hi"></div>
</div>
<style>
    .template{
        box-sizing:border-box;
        font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';
    }
</style>
