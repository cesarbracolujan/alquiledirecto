@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-banner-wrapper">
                    <div class="pt-4 pb-2">
                        <p><strong>Estimado usuario / adquiriente / inquilino:</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1">Para Ud. que se encuentra en la búsqueda de un lugar apropiado para vivir, trabajar o ampliar su negocio, Alquile Directo le brinda en forma gratuita la posibilidad de encontrar el inmueble ideal en la ubicación que siempre estuvo buscando.</li>
                        </ul>
                    </div>
                    <div class="pt-4 pb-4">
                        <p class="pb-2"><strong>Encuentre fácilmente la propiedad que busca vía nuestra web:</strong></p>
						<ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Se registra con una cuenta de correo electrónico. Gratis.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Marca como favorita y visualiza en sus favoritos, las propiedades que le interesa alquilar.</li>
							<li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Pueden compartir un resumen de la publicación en sus redes sociales.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>Oferte por la propiedad que más se ajuste a sus necesidades:</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Obtiene notificaciones cuando la propiedad esté disponible.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Se puede poner en contacto directo con el propietario.</li>
                        </ul>
                    </div>
					<div class="pb-4">
                        <p class="pb-2"><strong>Servicios especializados de Alquile Directo:</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Contratar el trámite notarial para la firma del contrato de alquiler.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Asesoría en tipos de contratos que aplican según la modalidad, etc.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
