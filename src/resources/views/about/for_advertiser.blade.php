@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-banner-wrapper">
                    <div class="pt-4 pb-4">
                        <p class="pb-2"><strong>Estimado anunciante:</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1">Alquile Directo es una propuesta nueva que le permite manejar desde un solo lugar todas sus publicaciones de alquiler de inmuebles, ya sea Ud. el propietario o un agente inmobiliario que desea publicitar los inmuebles que tenga para alquilar.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>Administre sus propiedades:</strong></p>
						<ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Todo propietario o agente inmobiliario registra su propiedad con un nombre (usuario).</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Publica el precio deseado, marca el periodo que esta alquilado quedando libre en el calendario las fechas que se encuentra disponible el inmueble.</li>
							<li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Puede activar o no el aviso en cualquier momento. Se da de baja si la propiedad se vende.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Puede ver las estadísticas de la publicación, cuantos marcaron como favoritas, ofertas recibidas, etc.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>Publicite sus propiedades permanentemente:</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> El aviso tiene una duración de un año.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Puede subir sus propias fotos del inmueble y/o modificar el texto de la publicación en cualquier momento.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> El aviso se llena según el perfil establecido por tipo de inmueble y se incluyen las reglas de convivencia.</li>
                        </ul>
                    </div>
					<div class="pb-4">
                        <p class="pb-2"><strong>Obtenga el mejor valor de renta:</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Ud. como propietario/agente inmobiliario puede cerrar la negociación al contactar con los interesados directamente.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> La publicación muestra el número de ofertas recibidas, usuarios que marcaron como favorita, etc.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>Marketing digital:</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Se puede adicionar el servicio de destaque en la misma página web. Así como en redes sociales.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> E-mailing a los usuarios usuarios/inquilinos registrados en AD.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>Servicios especializados de AD:</strong></p>
						<ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Publicación de avisos con la visita de uno de nuestros asesores, fotos 360, elaboración de planos 2D y descripción.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Asesoría en contratos, guía para alquilar, valores referenciales, impuestos, etc.</li>
							<li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Servicio de verificación, tramite notarial de contratos, etc.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Vía skype asesoramos al propietario a optimizar la información del inmueble.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Servicio para mostrar el inmueble a los posibles interesados.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
