@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-banner-wrapper">
                    <div class="small-section">
                        <h4 class="text-center">NOS ENCONTRAMOS EN PRE-LANZAMIENTO</h4>
                        <h4 class="text-center">PUEDES PUBLICAR TUS ANUNCIOS TOTALMENTE GRATIS</h4>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>¿Buscas un lugar para alquilar?</strong></p>
                        <p class="text-justify">Aquí podrás encontrar fácilmente la mejor propiedad en el área de tu interés. Pudiendo elegir si deseas alquilar por año, meses, semanas, días, temporadas, etc.</p>
                        <p class="text-justify">Nuestro objetivo es que puedas ubicar fácilmente una propiedad en alquiler y poder mudarte en forma ágil y sin complicaciones al inmueble que siempre buscaste.</p>
                        <p class="text-justify">Si estás buscando un lugar para alquilar puedes:</p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Encontrar fácilmente la propiedad vía web.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Puedes ver las fechas que se encuentran disponibles permitiendo que puedas programar tu mudanza.</li>
                        </ul>
                    </div>
                    <div class="">
                        <p class="pb-2"><strong>¿Buscas un sitio web para publicar tus avisos de alquiler?</strong></p>
                        <p class="text-justify">Somos la mejor opción en donde podrás administrar fácilmente todas tus publicaciones y contactar directamente a los posibles interesados.</p>
                        <p class="text-justify">Mantente siempre listo para promocionar un inmueble y amplia tus opciones de búsqueda de la persona o empresa que esté interesada en alquilar tú inmueble.</p>
                        <p class="text-justify">Si eres propietario y/o asesor inmobiliario puedes:</p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Administrar los periodos de alquiler de todas las propiedades.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Publicar la propiedad permanentemente.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Obtener el mejor valor de renta.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Reducir los tiempos de espera entre el fin del contrato y un nuevo usuario/inquilino.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Contratar marketing digital para la propiedad.</li>
                        </ul>
                    </div>
                    <div class="small-section">
                        <h4 class="text-dark text-center">Somos Alquile Directo, tu sitio web donde encontraras el mejor inmueble para alquilar.</h4>
                        <h4 class="text-dark text-center">¡¡ Alquile Directo, búscalo, contrátalo y múdate ya !!</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
