@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-banner-wrapper">
                    <div class="small-section">
                        <h4 class="text-dark text-center font-weight-bold">PREGUNTAS FRECUENTES DE USO DEL PORTAL ALQUILE DIRECTO</h4>
                    </div>
                    <div class="pb-4">
                        <p class="text-dark"><strong>Para el usuario del servicio de alquiler (locatario, inquilino o arrendador):</strong></p>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>¿A quién se le denomina “Usuario”?</strong></p>
						<ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> A toda persona o empresa que se encuentre interesada en buscar un lugar para alquilar, ya sea por meses, años, días, temporadas, etc.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>¿Qué servicios puedo encontrar en AlquileDirecto como “Usuario”?</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Ponemos a tu disposición un portal que permite buscar y encontrar fácilmente publicaciones de inmuebles en alquiler.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Podrás crear una cuenta de usuario con contraseña para tu mayor seguridad.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Puedes marcar y guardar en tu usuario aquellas propiedades que sean de tu interés y podrás compararlas ayudándote a elegir la mejor opción para ti.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Podrás planificar mudarte ya que las publicaciones te muestran las fechas en que se encuentra disponibles y aquellas que se encuentran ocupadas.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Una vez que decidas cual es el inmueble que cumpla con tus necesidades podrás contactar directamente con el propietario o agente inmobiliario que ofrece en alquiler el inmueble.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Si deseas como opcional, AlquileDirecto te ofrece algunos servicios relacionados con el alquiler del inmueble.</li>
                        </ul>
                    </div>
					<div class="pb-4">
                        <p class="pb-2"><strong>¿Cuánto tiempo puedo tener marcados como “mis Favoritos” los inmuebles de mi interés?</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Los avisos que se publican en el portal tienen una duración de hasta un año, por lo que te permitirá tener el tiempo suficiente para marcar y contactar con los propietarios o agentes de los inmuebles de tu interés.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> El objetivo es que puedas encontrar la mejor opción en la ubicación y características que mejor se ajuste a sus necesidades.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Si la publicación expira, los inmuebles que fueron marcados como favoritos ya no estarán disponibles.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"> <strong>¿Cómo puedo contactarme con el propietario o agente inmobiliario?</strong></p>                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Para contactarse con el propietario o agente inmobiliario, tenemos una opción en la página principal de la publicación en donde se muestra los datos de contacto.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Alquiledirecto promueve el contacto directo entre anunciantes y usuarios para que puedan llegar a un acuerdo.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>¿Cómo puedo contratar los servicios adicionales de Alquiledirecto?</strong></p>
						<ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Contáctenos vía telefónica, correo electrónico, haciendo referencia al servicio que desea adquirir y se realizara previa coordinación.</li>
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> Todo servicio está sujeto a un tarifario según el caso.</li>
                        </ul>
                    </div>
                    <div class="pb-4">
                        <p class="text-dark"><strong>Para el anunciante de publicaciones de alquiler de inmuebles (propietario, agente o arrendatario):</strong></p>
                    </div>
                    <div class="pb-4">
                        <p class="pb-2"><strong>¿A quién se le denomina “Anunciante”?</strong></p>
                        <ul class="text-secondary">
                            <li class="pl-5 w-100 mb-1"><i class="fa fa-chevron-right text-dark" aria-hidden="true">
                            </i> A toda persona o empresa que es propietario o agente que desea alquilar un inmueble, para ello está buscando una manera efectiva de administrar las publicaciones y la disponibilidad de los inmuebles.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
