@extends('layouts.app')
@section('content')       

            <!-- TÉRMINOS -->

            <div class="col-md-12">

            <div class="aDivTexto aPl-60 aListIcon aTa-j aW-73 aW-100-991 aPl-0-991 aPb-50-991 aDivTextHTML">
                <p style="text-align:center"><strong>Términos y Condiciones de uso</strong></p>

<h5 style="text-align:justify">1. Acerca del portal web AlquileDirecto.com</h5>

<p style="text-align:justify">Nuestros portales web, www.alquiledirecto.com, www.alquiledirecto.net, www.alquiledirecto.org, www.alquiledirecto.info, www.alquiledirecto.pe, www.alquiledirecto.com.pe, www.alquiledirecto.net.pe, www.alquiledirecto.nom.pe, www.alquiledirecto.org.pe en adelante denominados el “<strong>PORTAL</strong>”, los cuales son administrados por la empresa Respuesta Digital EIRL con número de RUC: 20601400554 en adelante denominada “<strong>REDIG</strong>”, el cual declara los siguientes términos y condiciones de uso que tienen por finalidad regular el acceso y utilización del Portal por parte de los usuarios.</p>

<h5 style="text-align:justify">2. Aceptación de las condiciones por parte del Usuario</h5>

<ul>
	<li style="text-align:justify">Toda persona natural o jurídica que ingrese a navegar y/o utilizar alguno de los servicios que ofrece el Portal adquiere automáticamente la condición de “<strong>USUARIO</strong>”.</li>
	<li style="text-align:justify">El acceso y navegación dentro del Portal por parte del Usuario tiene carácter libre y gratuito con excepción de los servicios que expresamente se indiquen como pagados.</li>
	<li style="text-align:justify">Del mismo modo los servicios que sean proporcionados por terceros a través del Portal están sujetas a condiciones específicas según sea su naturaleza.</li>
	<li style="text-align:justify">Al acceder al Portal, el Usuario declara ser mayor de 18 años de edad y encontrarse facultado a asumir obligaciones vinculantes con respecto a cualquier tipo de responsabilidad que se produzca por el uso del Portal.</li>
	<li style="text-align:justify">El acceso y navegación por parte del Usuario en el Portal implica la aceptación sin reservas de todas las disposiciones incluidas en el presente documento de Términos y Condiciones de Uso.</li>
</ul>

<h5 style="text-align:justify">3. Términos de uso y fines del Portal por parte del Usuario</h5>

<ul>
	<li style="text-align:justify">Los servicios que se ofrecen a través del Portal se encuentran disponibles sólo para aquellas personas que puedan celebrar contratos legalmente vinculantes de acuerdo a lo establecido por la ley aplicable.</li>
	<li style="text-align:justify">El Usuario se compromete a utilizar el Portal de conformidad con la Ley, la moral, las buenas costumbres y el orden público.</li>
	<li style="text-align:justify">El Usuario se obliga a no utilizar el Portal con fines o efectos ilícitos o contrarios al contenido de los presentes Términos y Condiciones de uso, lesivos de los intereses o derechos de terceros, o que de cualquier forma pueda dañar, inutilizar, deteriorar la plataforma o impedir un normal uso del Portal por parte de otros Usuarios.</li>
	<li style="text-align:justify">El Usuario se compromete expresamente a no destruir, alterar, inutilizar o, de cualquier otra forma, dañar los datos, programas o documentos electrónicos y demás que se encuentren en el Portal.</li>
	<li style="text-align:justify">El Usuario se compromete a no obstaculizar el acceso a otros Usuarios mediante el consumo masivo de los recursos informáticos a través de los cuales REDIG presta el servicio, así como a no realizar acciones que dañen, interrumpan o generen errores en dichos sistemas o servicios.</li>
</ul>

<ul>
	<li style="text-align:justify">El Usuario se compromete a no intentar penetrar o probar la vulnerabilidad de un sistema o de una red propia del Portal, así como quebrantar las medidas de seguridad o de autenticación del mismo.</li>
</ul>

<ul>
	<li style="text-align:justify">El Usuario se compromete a hacer un uso adecuado de los contenidos que se ofrecen en el Portal y a no emplearlos para incurrir en actividades ilícitas, así como a no publicar ningún tipo de contenido ilícito.</li>
	<li style="text-align:justify">El Usuario se compromete a no utilizar el Portal para, a modo de referencia, más no limitativo, enviar correos electrónicos masivos (spam) o correos electrónicos con contenido amenazante, abusivo, hostil, ultrajante, difamatorio, vulgar, obsceno o injurioso.</li>
	<li style="text-align:justify">El usuario se compromete a no utilizar un lenguaje ilícito, abusivo, amenazante, obsceno, vulgar, racista, ni cualquier lenguaje que se considere inapropiado, ni anunciar o proporcionar enlaces a sitios que contengan materia ilegal u otro contenido que pueda dañar o deteriorar la red personal o computadora de otro Usuario.</li>
</ul>

<h5 style="text-align:justify">4. Responsabilidad de administración del Portal por parte de REDIG.</h5>

<ul>
	<li style="text-align:justify">REDIG se reserva la potestad de determinar a su libre criterio, cuándo se produce la vulneración de cualquiera de los preceptos enunciados en el presente documento de Términos y Condiciones de uso por parte de los contenidos publicados por algún Usuario, así como la potestad de eliminar dichos contenidos del Portal.</li>
	<li style="text-align:justify">Todo Usuario tiene la posibilidad de denunciar cualquier conducta o contenido de otros Usuarios que considere vulneran sus derechos o los preceptos establecidos anteriormente.</li>
	<li style="text-align:justify">El Usuario podrá reportar cualquiera de los casos mencionados a través del siguiente correo electrónico: &nbsp;<a href="mailto:ayuda.usuarios@alquiledirecto.com">ayuda.usuarios@alquiledirecto.com</a></li>
	<li style="text-align:justify">En el caso en que un Usuario infrinja lo establecido en el presente documento de Términos y Condiciones de uso, REDIG procederá a realizar alguna de las siguientes acciones, dependiendo de la gravedad o reiteración de la infracción:</li>
</ul>

<p style="margin-left:80px; text-align:justify">1. Amonestación al Usuario.</p>

<p style="margin-left:80px; text-align:justify">2. Suspensión temporal de la cuenta del Usuario.</p>

<p style="margin-left:80px; text-align:justify">3. Cancelación definitiva de la cuenta del Usuario.</p>

<p style="margin-left:80px; text-align:justify">4. Acciones por responsabilidades civiles o penales.</p>

<ul>
	<li style="text-align:justify">Los pagos realizados por el Usuario y que han sido interrumpidos por alguna de las infracciones antes mencionadas no serán reembolsables.</li>
</ul>

<h5 style="text-align:justify">5. Responsabilidad del Usuario sobre el registro y manejo de contraseñas</h5>

<ul>
	<li style="text-align:justify">El Usuario podrá navegar por el Portal sin necesidad de registrarse en una cuenta, sin embargo, para determinados servicios que ofrece el Portal, podrá hacerlo ingresando sus datos personales en el formulario de registro implementado a tal efecto.</li>
	<li style="text-align:justify">El procedimiento de registro en el Portal es totalmente gratuito. La cuenta de Usuario no debe incluir el nombre de otra persona con la intención de hacerse pasar por esa persona, ni ser ofensivo, vulgar, obsceno o contrario a la moral y las buenas costumbres.</li>
	<li style="text-align:justify">Los Usuarios registrados contarán con una clave personal o password con el cual podrán acceder a su cuenta personal. Cada Usuario es responsable de su propia contraseña, y deberá mantenerla bajo absoluta reserva y confidencialidad, sin revelarla o compartirla, en ningún caso, con terceros.</li>
	<li style="text-align:justify">Cada Usuario es responsable de todas las acciones realizadas mediante el uso de su contraseña. Toda acción realizada a través de la cuenta personal de un Usuario se presume realizada por el Usuario titular de dicha cuenta.</li>
	<li style="text-align:justify">En el caso de que un Usuario identificara que un tercero conociera y usara su contraseña y su cuenta personal, deberá notificarlo de manera inmediata a <a href="mailto:ayuda.usuarios@alquiledirecto.com">ayuda.usuarios@alquiledirecto.com</a></li>
	<li style="text-align:justify">REDIG no será responsable de cualquier daño relacionado con la divulgación del nombre de un Usuario o de su contraseña, o del uso que cualquier persona de al nombre de un Usuario o contraseña.</li>
	<li style="text-align:justify">REDIG puede requerir el cambio de un nombre de Usuario y contraseña cuando considere que la cuenta ya no es segura, o si se recibe alguna queja o denuncia respecto al nombre de un Usuario que viole derechos de terceros.</li>
	<li style="text-align:justify">REDIG, puede rechazar el registro de un Usuario, cancelar su cuenta o no permitir el acceso a los servicios que ofrece el Portal, a modo de referencia, mas no limitativo, en caso se incumpla con los presentes Términos y Condiciones de uso o se incurra en posibles infracciones de carácter legal o se efectúe un uso inadecuado del Portal.</li>
</ul>

<h5 style="text-align:justify">6. Responsabilidad del Portal sobre las Publicaciones</h5>

<ul>
	<li style="text-align:justify">El portal pone a disposición de los Usuarios un espacio virtual que les permite, mediante Internet, alquilar inmuebles de todo tipo. Previo registro los Usuarios en el Portal podrán publicar avisos dentro del portal, ellos serán contactados por los posibles interesados por medio de los datos de contacto ingresados en el sitio y/o herramientas para estos fines.</li>
	<li style="text-align:justify">Queda expresamente establecido que el Portal no es el propietario de los artículos ofrecidos, no tiene posesión de ellos ni los ofrece en venta.</li>
	<li style="text-align:justify">El portal no interviene en el perfeccionamiento de las operaciones realizadas entre los usuarios ni en las condiciones por ellos estipuladas para las mismas, por ello no será responsable respecto de la existencia, calidad, cantidad, estado, integridad o legitimidad de los bienes ofrecidos, adquiridos o enajenados por los usuarios, así como de la capacidad para contratar de los usuarios o de la veracidad de los datos personales por ellos ingresados.</li>
	<li style="text-align:justify">Cada usuario conoce y acepta ser el exclusivo responsable por los artículos que publica para su alquiler y/o venta y por las ofertas y/o compras que realiza.</li>
</ul>

<h5 style="text-align:justify">7. Términos y condiciones de las publicaciones</h5>

<ul>
	<li style="text-align:justify">El Portal permite publicar avisos previo pago, sin embargo como parte de las campañas promocionales del Portal, se podrá entregar bonos de descuento con el fin que los usuarios puedan usar los primeros servicios en forma gratuita.</li>
	<li style="text-align:justify">Con el fin de publicar el anuncio de alquiler de un inmueble, el Usuario deberá estar registrado en el sitio web. Luego podrá seguir con el proceso de publicación seleccionando el tipo de aviso e ingresando los datos del inmueble.</li>
	<li style="text-align:justify">Dentro de las opciones de publicación puede elegir destacar su publicación, para ello selecciona el monto que invertirá en publicidad digital.</li>
	<li style="text-align:justify">Al finalizar el proceso de elaboración del anuncio, procederá al pago de los servicios contratados pudiendo elegir el tipo de comprobante y medio de pago.</li>
</ul>

<h5 style="margin-left:40px; text-align:justify">7.1. Destaques</h5>

<ul>
	<li style="text-align:justify">El Portal ofrece distintos tipos de destaques de acuerdo a la cantidad de tiempo que desee resaltar su anuncio, así como el tipo de medio donde desea realizar una publicidad adicional para su anuncio.</li>
	<li style="text-align:justify">Las opciones son a través de ubicaciones seleccionadas dentro del Portal, a través de redes sociales y mailing a los usuarios registrados en Alquile Directo. El servicio de destacar un anuncio se hará por cada inmueble y anuncio registrado en el Portal.</li>
	<li style="text-align:justify">Al término del tiempo contratado el anuncio saldrá de la sección de destacados automáticamente y/o destaque del anuncio en redes sociales y mailing contratado.</li>
	<li style="text-align:justify">El usuario acepta que en ningún caso procederá a la devolución de su dinero, descuentos, reembolsos prorrateados, ni similares, una vez adquirido cualquiera de los destaques o paquetes, siempre que su aviso sea publicado en cualquiera de las opciones disponibles.</li>
	<li style="text-align:justify">Todo reclamo con respecto al pago de alguno de los destaques ofrecidos en el sitio deberá realizarse presentando la boleta de pago o el estado de cuenta en dónde se verifique el depósito, según el medio de pago escogido.</li>
	<li style="text-align:justify">Por ningún motivo, incluido las fallas en los sistemas operativos o al sitio, se generará el cobro de un lucro cesante a favor del usuario.</li>
</ul>

<h5 style="margin-left:40px; text-align:justify">7.2. Tiempo de Expiración</h5>

<ul>
	<li style="text-align:justify">El tiempo de publicación máximo de los avisos es de un año y depende del tiempo que haya contratado el Usuario. Se considera el tiempo de vigencia desde la activación del anuncio.</li>
	<li style="text-align:justify">Después del período contratado, todos los anuncios, serán considerados como inactivos y no podrán recibir ofertas o preguntas por parte de los Usuarios interesados. El Usuario podrá activarlos &nbsp;nuevamente considerando las tarifas establecidas.</li>
	<li style="text-align:justify">El Usuario podrá desactivar desde su cuenta una publicación antes de la fecha de vencimiento establecida.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.3. Emisión de Comprobantes</h5>

<ul>
	<li style="text-align:justify">El portal permite seleccionar comprobantes tales como Boleta o Factura. Para este fin el usuario deberá seleccionar antes de realizar el pago el tipo de comprobante deseado e ingresar los datos correspondientes al mismo.</li>
	<li style="text-align:justify">En caso el usuario no ingrese dato alguno o indique el tipo de comprobante deseado, según el canal de publicación, el comprobante será emitido como Boleta de Venta, sin lugar a reclamo posterior.</li>
	<li style="text-align:justify">Sobre los datos de facturación ingresados, estos se mantendrán fijos y no podrán ser modificados por el usuario desde su cuenta. Para modificaciones tendrá que enviar una solicitud a la Central de Ayuda correo <a href="mailto:ayuda.usuarios@alquiledirecto.com">ayuda.usuarios@alquiledirecto.com</a></li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.4. Descripciones</h5>

<ul>
	<li style="text-align:justify">Las descripciones no podrán contener información falsa sobre el producto ni sobre sus características y estado. No podrán incluir palabras que no estén relacionadas con el producto o servicio en cuestión, con el único fin de direccionar hacia el aviso los procesos de búsqueda realizados en el Portal.</li>
	<li style="text-align:justify">Si se incluye una fotografía, esta deberá ser indefectiblemente la correspondiente al producto o servicio ofertado, además no está permitido que la foto contenga texto ni un conjunto de fotos dentro del único espacio habilitado para esta opción, es decir,&nbsp; fotos de tipo collage.</li>
	<li style="text-align:justify">Queda terminantemente prohibida la utilización de códigos en las descripciones con la función de colocar y leer cookies, re-direccionar a los Usuarios hacia páginas externas a el Portal, abrir ventanas adicionales, solicitar u obtener cualquier tipo de información de otros Usuarios, etc. El incumplimiento de esta cláusula, puede acarrear la eliminación del o los mensajes, así como la inhabilitación de la cuenta, sin que ello genere algún tipo de indemnización para el usuario.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.5. Comparaciones</h5>

<ul>
	<li style="text-align:justify">Queda terminantemente prohibida la publicación en el Portal de avisos que establezcan comparaciones con otros usuarios o con avisos de terceros o bien contengan información referida a otros avisos (nombres, apodos, precios, cantidades, etc.).</li>
</ul>

<ul>
	<li style="text-align:justify">El incumplimiento de esta cláusula, puede acarrear la eliminación del o los mensajes, así como la inhabilitación de la cuenta, sin que ello genere algún tipo de indemnización para el usuario.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.6. Copia de descripciones y/o fotos</h5>

<ul>
	<li style="text-align:justify">El Portal no permite el uso ilícito de la copia de descripciones y/o de fotos protegidos por derechos de autor. Tanto la descripción del artículo como las fotos que incluya en el mismo, deberán ser autorizadas por los titulares de los derechos de autor.</li>
	<li style="text-align:justify">Todas las reproducciones de descripciones y/o fotografías no autorizadas por los titulares de los derechos de autor serán retiradas inmediatamente del Portal sin lugar a reclamo por parte del usuario, previa verificación de la titularidad correspondiente.</li>
	<li style="text-align:justify">El Portal se reserva el derecho de retirar tanto fotos como avisos que incluyan imágenes no autorizadas y/o bloquear usuario.</li>
	<li style="text-align:justify">De verificarse algún dato falso o que no corresponda al alquiler del inmueble, en el precio, descripción u otro dato del aviso, el Portal procederá a retirar el aviso y/o bloquear al usuario del Sitio.</li>
	<li style="text-align:justify">El equipo de moderación de Alquile Directo utilizará canales respectivos para la verificación de dichos datos.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.7. Ubicación de avisos</h5>

<ul>
	<li style="text-align:justify">El usuario deberá ofrecer el alquiler de los bienes y/o servicios en la categoría que corresponda, sin repetir sus productos y/o servicios en otras categorías.</li>
	<li style="text-align:justify">El Portal se reserva el derecho de modificar la categoría en la que se haya publicado un aviso en caso que así se lo crea conveniente, pudiendo incluso procederse a la eliminación del aviso en cuestión.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.8. Un bien y/o servicio por aviso</h5>

<ul>
	<li style="text-align:justify">Cada aviso tendrá asociado la oferta de un bien y/o servicio, no está permitido repetir el mismo aviso en la misma categoría o en categorías que no correspondan y/o crear cuentas con el propósito de repetir un mismo aviso con el fin de beneficiarse con la publicación de avisos tipo 'gratuito' a costa del Portal y los límites establecidos con respecto a la publicación de avisos gratuitos.</li>
	<li style="text-align:justify">En caso de no respetar esta cláusula, queda a criterio del Portal, aplicar sanciones como la eliminación del mensaje y/o registro de usuario, sin que ello represente alguna indemnización para el usuario.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.9. Evasión Fiscal</h5>

<ul>
	<li style="text-align:justify">El precio de los productos y servicios publicados deberá ser siempre el precio final del mismo, por lo que tendrá que incluir el valor del IGV siempre que corresponda su aplicación. El Portal, podrá corregir cualquier aviso cuyo precio no cumpla con este requerimiento, con el fin de evitar infracciones publicitarias relacionadas con el precio final del producto.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.10. Lenguaje vulgar</h5>

<ul>
	<li style="text-align:justify">Queda terminantemente prohibido el uso de lenguaje vulgar, blasfemo, obsceno, racista, sexual u ofensivo.</li>
	<li style="text-align:justify">Esta política se aplica en todo el sitio, incluyendo también el intercambio de mensajes entre usuarios, como el que se puede establecer en el envío de preguntas y respuestas; y en el intercambio de mensajes usuario - personal del Portal, como el que se puede establecer al realizar verificaciones de las publicaciones.</li>
	<li style="text-align:justify">El Portal se reserva el derecho de bloquear al usuario que incurra en dicho tipo de agresiones.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.11. Regalos, Rifas y Premios</h5>

<ul>
	<li style="text-align:justify">Las publicaciones que incluyan publicidad sobre bonificaciones, regalos, sorteos o premios aleatorios como incentivo para los compradores no están permitidas en el Portal.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.12. Fallas del Sistema</h5>

<ul>
	<li style="text-align:justify">Los Usuarios NO podrán imputarle al Portal responsabilidad alguna ni exigir pago por lucro cesante, en virtud de perjuicios resultantes de dificultades técnicas o fallas en los sistemas o en Internet.</li>
	<li style="text-align:justify">El Portal no garantiza el acceso y uso continuado o ininterrumpido de su sitio. El sistema o algún aviso, pueden eventualmente no estar disponible debido a dificultades técnicas o fallas de Internet, o por cualquier otra circunstancia ajena al Portal; en tales casos se procurará restablecerlo con la mayor celeridad posible sin que por ello pueda imputársele algún tipo de responsabilidad.</li>
	<li style="text-align:justify">El Portal no será responsable por ningún error u omisión contenido en su sitio web.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.13. Artículos Prohibidos</h5>

<ul>
	<li style="text-align:justify">Queda terminantemente prohibida la oferta y/o comercialización de todo tipo de inmuebles que no sean para alquiler. Así como la publicación de avisos cuyo único fin es ofrecer servicios para la búsqueda externa de inmuebles en venta y/o alquiler.</li>
	<li style="text-align:justify">Asimismo, los bienes y/o servicios relacionados con respecto a inmuebles, (como por ejemplo decoración, servicios de gasfitería o de electricidad, etc.), sólo podrán ser comercializados dentro de los categorías designadas por el Portal, en su debido momento según indique el portal.</li>
	<li style="text-align:justify">Si un aviso, no se adecua a la categoría correcta, dicho aviso podrá ser eliminado por el Portal, sin que ello genere algún tipo de compensación para el Usuario.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="margin-left:40px; text-align:justify">7.14. Medidas aplicadas por el Portal</h5>

<ul>
	<li style="text-align:justify">El Portal podrá editar, modificar, desactivar y/o eliminar al aviso(s) y/o usuario(s) que incumplan las políticas especificadas en párrafos anteriores o la ley vigente, sin que de esto cree ninguna obligación de resarcimiento a favor del usuario, ni reembolso del dinero en caso haya adquirido un destaque.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">8. Propiedad Intelectual</h5>

<ul>
	<li style="text-align:justify">Todos los derechos de propiedad intelectual del Portal y de sus contenidos y diseños pertenecen a REDIG o, en su caso, a terceras personas. En aquellos casos en que sean propiedad de terceros contamos con las licencias necesarias para su utilización.</li>
	<li style="text-align:justify">Quedan expresamente prohibidas la reproducción, la distribución, transformación, la comunicación pública y puesta a disposición, de la totalidad o parte de los contenidos del Portal, en cualquier soporte y por cualquier medio técnico, sin la autorización de REDIG.</li>
	<li style="text-align:justify">El Usuario se compromete a respetar los derechos de propiedad industrial e intelectual de titularidad de REDIG y de terceros.</li>
	<li style="text-align:justify">Asimismo, queda expresamente prohibido la utilización o reproducción de cualquier marca registrada, nombre o logotipo que figure en el Portal sin la autorización previa y por escrito de REDIG, así como la utilización del software que opera el Portal con excepción de aquellos usos permitidos bajo estos Términos y Condiciones de uso.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">9. Contenidos publicados por los Usuarios</h5>

<ul>
	<li style="text-align:justify">REDIG no garantiza la veracidad, exactitud, exhaustividad y actualidad de los contenidos publicados por los Usuarios en el Portal. Es responsabilidad del Usuario garantizar la autenticidad, veracidad y actualización de la información que publica en el Portal. En cualquier caso el Usuario será el único responsable de las manifestaciones falsas o inexactas que realice y de los perjuicios que pudiera causar al Portal o a terceros por la información que facilite.</li>
	<li style="text-align:justify">REDIG no se hace responsable por los contenidos ya sean estos, imágenes, fotos, textos o cualquier otro tipo de material digital, publicados por terceros en el Portal y traslada toda responsabilidad a cada Usuario proveedor de dichos contenidos.</li>
	<li style="text-align:justify">El Usuario deberá ofrecer, en alquiler o venta, los bienes y/o servicios en la categoría y subcategorías apropiadas. Las publicaciones podrán incluir textos descriptivos, gráficos, fotografías y otros contenidos, siempre que no violen ninguna disposición de los presentes Términos y Condiciones de uso o la Política de Publicación del Portal.</li>
	<li style="text-align:justify">El producto o servicio ofrecido por el Usuario debe ser exactamente descrito en cuanto a sus condiciones y características relevantes. En el caso de que se incluya una fotografía, esta deberá corresponder específicamente al artículo que se ofrece, salvo que se trate de bienes o servicios que por su naturaleza no permiten esa correspondencia.</li>
	<li style="text-align:justify">Se entiende y presume que mediante la inclusión de un bien o servicio en el Portal, el Usuario tiene la intención y el derecho de alquilar el bien por él ofrecido, o está facultado para ello por su titular y lo tiene disponible para su entrega inmediata. Se establece que los precios de los productos publicados deberán ser expresados con IGV incluido cuando corresponda la aplicación del mismo.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">10. Enlaces de terceros</h5>

<ul>
	<li style="text-align:justify">En el supuesto de que en el Portal se dispusieran enlaces o hipervínculos hacia otros sitios de Internet, REDIG declara que no ejerce ningún tipo de control sobre dichos sitios y contenidos.</li>
	<li style="text-align:justify">En ningún caso REDIG asumirá responsabilidad alguna por los contenidos de algún enlace perteneciente a una web ajena, ni garantizará la disponibilidad técnica, calidad, fiabilidad, exactitud, veracidad, validez y constitucionalidad de cualquier material o información contenida en cabeza de los hipervínculos u otros lugares de Internet.</li>
	<li style="text-align:justify">Estos enlaces se proporcionan únicamente para informar al Usuario sobre la existencia de otras fuentes de información sobre un tema concreto, y la inclusión de un enlace no implica la aprobación de la página web enlazada por parte de REDIG.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">11. Modificación de los Términos y Condiciones</h5>

<ul>
	<li style="text-align:justify">REDIG se reserva expresamente el derecho a modificar, actualizar o ampliar en cualquier momento los presentes Términos y Condiciones de uso.</li>
	<li style="text-align:justify">Cualquier modificación, actualización o ampliación producida en los presentes Términos y Condiciones de uso será inmediatamente publicada siendo responsabilidad del Usuario revisar los Términos y Condiciones de uso vigentes al momento de la navegación.</li>
	<li style="text-align:justify">Cada Usuario tendrá el derecho de no aceptar las variaciones de los presentes Términos y Condiciones de uso, en cuyo caso deberá informar a REDIG. De verificarse el rechazo por parte de un Usuario a tales modificaciones, se le dará inmediatamente de baja como tal, inhabilitándolo en adelante para la utilización del servicio.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">12. Limitación de responsabilidad e indemnidad</h5>

<ul>
	<li style="text-align:justify">Salvo que así lo establezca la legislación aplicable de obligado cumplimiento, el uso que el Usuario haga del Portal o de todas las funcionalidades que el Portal ofrece, incluyendo cualquier contenido, publicación o herramienta contenida en la misma, se ofrece “tal cual” y “según su disponibilidad” sin declaraciones o garantías de ningún tipo, tanto explícitas como implícitas, incluidas entre otras, las garantías de comerciabilidad, adecuación a un fin particular y no incumplimiento.</li>
	<li style="text-align:justify">REDIG no garantiza que el Portal sea siempre seguro o esté libre de errores, ni que funcione siempre sin interrupciones, retrasos o imperfecciones.</li>
	<li style="text-align:justify">REDIG no se hace responsable de los posibles daños o perjuicios en el Portal, que se puedan derivar de interferencias, omisiones, interrupciones, virus informáticos, averías o desconexiones en el funcionamiento operativo de este sistema electrónico, de retrasos o bloqueos en el uso de este sistema electrónico causados por deficiencias o sobrecargas en el sistema de Internet o en otros sistemas electrónicos, así como también de daños que puedan ser causados por terceras personas mediante intromisiones ilegítimas fuera del control de REDIG.</li>
	<li style="text-align:justify">Asimismo, REDIG no se hace responsable por la calidad, utilidad e idoneidad de los productos o servicios contratados por el Usuario en el Portal. En este sentido, REDIG no responderá por las posibles reclamaciones que puedan formular los Usuarios relacionadas con la calidad o adecuación de los productos o bienes contratados con los anunciantes.&nbsp;</li>
	<li style="text-align:justify">El Usuario conoce y acepta que al realizar operaciones con otros Usuarios o terceros lo hace bajo su propia cuenta y riesgo, y se compromete a mantener al margen a REDIG, sus directivos, empleados, representantes y apoderados de todo reclamo que pueda llegar a entablar en contra e otros Usuarios del Portal.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">13. Datos de Carácter Personal</h5>

<ul>
	<li style="text-align:justify">Los distintos tratamientos de datos personales que REDIG realiza a través del Portal, así como las finalidades de dichos tratamientos serán detallados específicamente en la&nbsp;Política de Privacidad&nbsp;del Portal a la que el Usuario podrá acceder a través del siguiente enlace: Política de Privacidad.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">14. Comunicaciones</h5>

<ul>
	<li style="text-align:justify">El Usuario acepta expresamente que la dirección de correo electrónico consignada en el formulario de registro será el medio de contacto oficial entre el Portal y el Usuario, siendo absoluta responsabilidad de este último verificar que dicho correo electrónico esté siempre activo y funcional para poder recibir todas las comunicaciones procedentes del Portal.</li>
	<li style="text-align:justify">Los mensajes o comunicaciones del Portal a los Usuarios sólo pueden provenir de las páginas o cuentas oficiales de éste en redes sociales u otros medios. En caso se detectara que algún Usuario está enviando comunicaciones o realizando publicaciones en nombre del Portal, REDIG iniciará las acciones correctivas y legales pertinentes a fin de proteger al resto de Usuarios de posibles riesgos de confusión.</li>
	<li style="text-align:justify">De otro lado, toda comunicación que el Usuario desee dirigir al Portal deberá realizarla a través de la siguiente dirección de correo electrónico:&nbsp;<a href="mailto:ayuda.usuarios@alquiledirecto.com">ayuda.usuarios@alquiledirecto.com</a></li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">15. Fuerza mayor</h5>

<ul>
	<li style="text-align:justify">REDIG no será responsable por cualquier retraso o falla en el rendimiento o la interrupción en la prestación de los servicios que pueda resultar directa o indirectamente de cualquier causa o circunstancia más allá de su control razonable, incluyendo, pero sin limitarse a fallas en los equipos o las líneas de comunicación electrónica o mecánica, robo, errores del operador, clima severo, terremotos o desastres naturales, huelgas u otros problemas laborales, guerras, o restricciones gubernamentales.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">16. Libro de reclamaciones</h5>

<ul>
	<li style="text-align:justify">Conforme a lo establecido en el Código de Protección y Defensa del Consumidor, Ley N° 29571, el Portal pone a disposición del Usuario un Libro de Reclamaciones virtual a fin de que éste pueda registrar sus quejas o reclamos formales sobre los servicios ofrecidos a través del Portal. Éste puede ser encontrado en esta dirección:&nbsp;xxxxxxxxxxxxxxxxxxxxxxxxxx</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">17. Autoridades y requerimientos legales</h5>

<ul>
	<li style="text-align:justify">REDIG coopera con las autoridades competentes y con terceros para garantizar el cumplimiento de las leyes en materia de protección de derechos de propiedad industrial e intelectual, prevención del fraude, protección al consumidor y otras materias.</li>
	<li style="text-align:justify">REDIG podrá revelar la información personal de Usuarios del Portal bajo requerimiento de las autoridades judiciales o gubernamentales competentes, en la medida en que discrecionalmente lo entienda necesario, para efectos de investigaciones conducidas por ellas, cuando se trate de investigaciones de carácter penal o de fraude, o las relacionadas con piratería informática, la violación de derechos de autor, infracción de derechos de propiedad intelectual o otra actividad ilícita o ilegal que pueda exponer al Portal o a sus Usuarios a cualquier responsabilidad legal.</li>
	<li style="text-align:justify">Asimismo, REDIG se reserva el derecho de comunicar información sobre sus Usuarios a otros Usuarios, a entidades o a terceros, cuando haya motivos suficientes para considerar que la actividad de un Usuario sea sospechosa de intentar o cometer un delito o intentar perjudicar a otras personas. Este derecho será utilizado por REDIG a su entera discreción cuando lo considere apropiado o necesario para mantener la integridad y seguridad del Portal y la de sus Usuarios, para hacer cumplir los presentes Términos y Condiciones de uso, y a efectos de cooperar con la ejecución y cumplimiento de la ley.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">18. Inexistencia de sociedad o relación laboral</h5>

<ul>
	<li style="text-align:justify">La participación de un Usuario en el Portal no constituye ni crea contrato de sociedad, de representación, de mandato, como así tampoco relación laboral alguna entre dicho Usuario y REDIG.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">19. Cesión de posición contractual</h5>

<ul>
	<li style="text-align:justify">Los Usuarios autorizan expresamente la cesión de estos Términos y Condiciones de uso y de su información personal en favor de cualquier persona que (i) quede obligada por estos Términos y Condiciones de uso y/o (ii) que sea el nuevo responsable del banco de datos que contenga su información personal.&nbsp; Luego de producida la cesión, REDIG no tendrá ninguna responsabilidad con respecto de cualquier hecho que ocurrirá a partir de la fecha de la cesión.&nbsp; El nuevo responsable del banco de datos asumirá todas y cada una de las obligaciones de REDIG establecidas en los presentes Términos y Condiciones de uso y en la Política de Privacidad respecto del tratamiento, resguardo y conservación de la información personal de los usuarios del Portal.</li>
</ul>

<p style="text-align:justify">&nbsp;</p>

<h5 style="text-align:justify">20. Ley aplicable y jurisdicción</h5>

<ul>
	<li style="text-align:justify">Los presentes Términos y Condiciones de uso se rigen por la ley peruana y cualquier disputa que se produzca con relación a la validez, aplicación o interpretación de los mismos, incluyendo la Política de Privacidad, será resuelta en los tribunales de Lima.</li>
</ul>

            </div>

            </div>

            <!-- POLÍTICAS -->

            <div class="col-md-12">

            <div class="aDivTexto aPl-60 aListIcon aTa-j aW-73 aW-100-991 aPl-0-991 aPb-50-991 aDivTextHTML">
                <p style="text-align: center;"><strong>Política de la Empresa</strong></p>

<p style="text-align: justify;">La presente política de privacidad de la empresa tiene por finalidad informar la manera como&nbsp;RESPUESTA DIGITAL E.I.R.L. en adelante REDIG y R.U.C. 20601400554, trata su información personal a través de los portales&nbsp;web, www.alquiledirecto.com, www.alquiledirecto.net, www.alquiledirecto.org, www.alquiledirecto.info, www.alquiledirecto.pe, www.alquiledirecto.com.pe, www.alquiledirecto.net.pe, www.alquiledirecto.nom.pe, www.alquiledirecto.org.pe en adelante denominados el “<strong>PORTAL</strong>”.</p>

<p style="text-align: justify;">La Política de privacidad describe toda la tipología de información personal que se recaba de los Usuarios, y todos los tratamientos que se realizan con dicha información.</p>

<p style="text-align: justify;">El Usuario declara haber leído y aceptado de manera previa y expresa la Política de privacidad sujetándose a sus disposiciones.</p>

<h5 style="text-align: justify;">1. ¿Qué información recolectamos?</h5>

<p style="text-align: justify;">Al momento de registrarse en el Portal solicitamos al Usuario datos personales que permiten identificarlo, contactarlo o localizarlo, tal como el nombre, dirección de correo electrónico, dirección postal, teléfono, entre otros.</p>

<p style="text-align: justify;">Asimismo, REDIG requiere almacenar información relativa al comportamiento del Usuario dentro del Portal, entre la que se incluye:</p>

<ul>
	<li style="text-align: justify;">La URL de la que proviene el Usuario (incluyendo las externas al Portal).</li>
	<li style="text-align: justify;">URLs más visitadas por el Usuario (incluyendo las externas al Portal).</li>
	<li style="text-align: justify;">Direcciones de IP. Navegador que utiliza el Usuario.</li>
	<li style="text-align: justify;">Todas las actividades realizadas dentro del Portal. Información sobre la operativa del portal, tráfico, promociones, campañas de venta, estadísticas de navegación, entre otros.</li>
</ul>

<h5 style="text-align: justify;">2. Sobre la veracidad de la Información que recolectamos</h5>

<p style="text-align: justify;">El Usuario, al registrarse y utilizar el Portal, declara que toda la información proporcionada es verdadera, completa y exacta. Cada Usuario es responsable por la veracidad, exactitud, vigencia y autenticidad de la información suministrada y se compromete a mantenerla debidamente actualizada.</p>

<p style="text-align: justify;">Sin perjuicio de lo anterior, el Usuario autoriza a REDIG a verificar la veracidad de los datos personales facilitados por el Usuario a través de información obtenida de fuentes de acceso público o entidades especializadas en la provisión de dicha información.</p>

<p style="text-align: justify;">REDIG no se hace responsable de la veracidad de la información que no sea de elaboración propia, por lo que tampoco asume responsabilidad alguna por posibles daños o perjuicios que pudieran originarse por el uso de dicha información.</p>

<h5 style="text-align: justify;">3. ¿Cómo conservamos su información personal?&nbsp;</h5>

<p style="text-align: justify;">De acuerdo a lo establecido en la Ley N° 29733, Ley de Protección de Datos Personales, y el Decreto Supremo N° 003-2013-JUS, por el que se aprueba el Reglamento de la Ley de Protección de Datos Personales, REDIG informa a los Usuarios del Portal que todos los datos de carácter personal que nos faciliten serán incorporados a un banco de datos, debidamente inscrito en la Dirección de Registro Nacional de Protección de Datos Personales, titularidad de REDIG.</p>

<p style="text-align: justify;">A través de la presente Política de Privacidad el Usuario da su consentimiento expreso para la inclusión de sus datos personales en el mencionado banco de datos.</p>

<h5 style="text-align: justify;">4. ¿Para qué utilizamos su información personal?</h5>

<p style="text-align: justify;">Los datos personales de los Usuarios del Portal son tratados con las siguientes finalidades:</p>

<ul>
	<li style="text-align: justify;">Atender y procesar solicitudes de registro de Usuarios en el Portal, brindar soporte al Usuario y validar la veracidad de la información de registro. &nbsp;</li>
	<li style="text-align: justify;">Gestionar y administrar las cuentas personales de los Usuarios, así como supervisar el comportamiento y la actividad del Usuario dentro del Portal.</li>
	<li style="text-align: justify;">Brindar los datos de contacto del anunciante o creador del aviso a otros Usuarios interesados. Toda la información obtenida con este fin sólo podrá utilizarse para concretar la operación que dio lugar al envío de estos datos, no pudiendo ser usada para ningún otro fin, salvo que se cuente con la autorización expresa del Usuario involucrado.</li>
	<li style="text-align: justify;">Contactar al Usuario interesado en utilizar algún servicio ofrecido por&nbsp;el Portal, así como absolver sus consultas.</li>
	<li style="text-align: justify;">Realizar estudios internos sobre los intereses, comportamientos y hábitos de conducta de los Usuarios a fin poder ofrecerles un mejor servicio de acuerdo a sus necesidades, con información que pueda ser de su interés.</li>
	<li style="text-align: justify;">Brindar información a los Usuarios sobre los nuevos servicios y promociones ofrecidos por el Portal y/o por los Usuarios anunciantes.&nbsp;</li>
	<li style="text-align: justify;">Gestionar los concursos y promociones que se realicen con los Usuarios registrados en el Portal.</li>
	<li style="text-align: justify;">Informar sobre los ganadores de premios, promociones, concursos y/o sorteos realizados por el Portal.</li>
	<li style="text-align: justify;">Los Usuarios que participen en las promociones, concursos o sorteos mencionados, autorizan expresamente a que el Portal difunda por los medios que estime convenientes los datos personales e imágenes de los ganadores, sin derecho a compensación alguna.</li>
	<li style="text-align: justify;">Compartir los datos personales de los Usuarios&nbsp;con terceros tales como empresas que contribuyan a mejorar o facilitar la operativa del Portal, servicios de transporte, medios de pago, proveedores de seguros, gestores, entre otros.</li>
	<li style="text-align: justify;">Adicionalmente, los datos del Usuario serán utilizados con la finalidad de enviarle noticias, promociones, publicidad y novedades del Portal a través de comunicaciones periódicas que serán remitidas a la dirección de correo electrónico que el Usuario facilitó al momento de realizar el registro. Dichas comunicaciones serán consideradas solicitadas y no califican como “spam” bajo la normativa vigente.&nbsp;</li>
</ul>

<p style="text-align: justify;">REDIG informa al Usuario que una vez autorizado el envío de las mencionadas comunicaciones, tendrá la facultad de revocar el consentimiento prestado en cada una de las comunicaciones que reciba de Alquile Directo a través de un hipervínculo que se incorpora en todas las comunicaciones, o enviando una solicitud a la siguiente dirección de correo electrónico:&nbsp;<a href="mailto:ayuda.usuarios@alquiledirecto.com">ayuda.usuarios@alquiledirecto.com</a></p>

<p style="text-align: justify;">El Usuario del Portal manifiesta expresamente que ha sido informado de todas las finalidades antes mencionadas y autoriza el tratamiento de sus datos personales&nbsp;con dichas finalidades.</p>

<p style="text-align: justify;">REDIG le recuerda al Usuario que las finalidades de tratamiento de datos necesarias para la ejecución de la relación contractual que vincula al Usuario registrado y a REDIG no requieren del consentimiento del mismo.</p>

<h5 style="text-align: justify;">5. ¿Cómo resguardamos&nbsp;su información personal?</h5>

<p style="text-align: justify;">REDIG, a través del Portal, adopta las medidas técnicas y organizativas necesarias para garantizar la protección de los datos de carácter personal y evitar su alteración, pérdida, tratamiento y/o acceso no autorizado, habida cuenta del estado de la técnica, la naturaleza de los datos almacenados y los riesgos a que están expuestos, todo ello, conforme a lo establecido por la Ley N° 29733, Ley de Protección de Datos Personales, su Reglamento y la Directiva de Seguridad.</p>

<p style="text-align: justify;">En este sentido, REDIG usará los estándares de la industria en materia de protección de la confidencialidad de la información personal de los Usuarios del Portal.</p>

<p style="text-align: justify;">REDIG emplea diversas técnicas de seguridad para proteger tales datos de accesos no autorizados. Sin perjuicio de ello, REDIG no se hace responsable por interceptaciones ilegales o violación de sus sistemas o bases de datos por parte de personas no autorizadas, así como la indebida utilización de la información obtenida por esos medios, o de cualquier intromisión ilegítima que escape al control de REDIG y que no le sea imputable.</p>

<p style="text-align: justify;">REDIG tampoco se hace responsable de posibles daños o perjuicios que se pudieran derivar de interferencias, omisiones, interrupciones, virus informáticos, averías telefónicas o desconexiones en el funcionamiento operativo de este sistema electrónico, motivadas por causas ajenas a REDIG; de retrasos o bloqueos en el uso de la plataforma informática causados por deficiencias o sobrecargas en el Centro de Procesos de Datos, en el sistema de Internet o en otros sistemas electrónicos.</p>

<h5 style="text-align: justify;">6. ¿Con quiénes compartimos información?</h5>

<p style="text-align: justify;">Forma parte de la actividad propia del Portal dar a conocer los datos de contacto del arrendatario y/o agente inmobiliario del inmueble y/o servicio. El resto de los datos personales recabados por&nbsp;el Portal en el proceso de registro o en cualquier otro momento, no serán suministrados a ningún otro Usuario.</p>

<p style="text-align: justify;">Los Usuarios podrán utilizar la mencionada información que el Portal les da a conocer únicamente para establecer comunicaciones relacionadas con los servicios del Portal,&nbsp;siempre que no sean comunicaciones comerciales no solicitadas.</p>

<p style="text-align: justify;">Queda terminantemente prohibido brindar información personal sobre otro Usuario del Portal sin que medie la expresa autorización por parte del Usuario en cuestión y la del Portal, incluso en los casos en que se haya realizado una operación comercial entre los Usuarios en cuestión.</p>

<p style="text-align: justify;">REDIG se compromete a no divulgar o compartir con terceros la información personal recabada de los Usuarios sin que se haya prestado el debido consentimiento para ello, con excepción de los siguientes casos:</p>

<ul>
	<li style="text-align: justify;">En aquellos casos en que el uso de los datos personales sea necesario para la prestación de los servicios brindados a través del Portal.</li>
	<li style="text-align: justify;">Solicitud de información de autoridades públicas en ejercicio de sus funciones y el ámbito de sus competencias.</li>
	<li style="text-align: justify;">Solicitud de información en virtud de órdenes judiciales.</li>
	<li style="text-align: justify;">Solicitud de información en virtud de disposiciones legales.</li>
</ul>

<h5 style="text-align: justify;">7. Cookies</h5>

<p style="text-align: justify;">El Portal utiliza cookies. Una “Cookie” es un pequeño archivo de texto que un sitio web almacena en el navegador del Usuario. Las cookies facilitan el uso y la navegación por una página web y son esenciales para el funcionamiento de Internet, aportando innumerables ventajas en la prestación de servicios interactivos.</p>

<p style="text-align: justify;">El Portal podrá utilizar la información de su visita para realizar evaluaciones y cálculos estadísticos sobre datos anónimos, así como para garantizar la continuidad del servicio o para realizar mejoras en el Portal.</p>

<p style="text-align: justify;">El Portal también utilizará la información obtenida a través las cookies para analizar los hábitos de navegación del Usuario y las búsquedas realizadas por éste, a fin de mejorar sus iniciativas comerciales y promocionales, mostrando publicidad que pueda ser de su interés, y personalizando los contenidos del Portal.</p>

<p style="text-align: justify;">Las cookies pueden borrarse del disco duro si el Usuario así lo desea. La mayoría de los navegadores aceptan las cookies de forma automática, pero le permiten al Usuario cambiar la configuración de su navegador para que rechace la instalación de cookies, sin que ello perjudique su acceso y navegación por el Portal.</p>

<p style="text-align: justify;">En el supuesto de que en el presente Portal se dispusieran enlaces o hipervínculos hacia otros lugares de Internet propiedad de terceros que utilicen cookies, REDIG no se hace responsable ni controla el uso de cookies por parte de dichos terceros.</p>

<h5 style="text-align: justify;">8. Compartir publicación del inmueble con un amigo</h5>

<p style="text-align: justify;">Si el Usuario elige el servicio “Envía esta publicación a un amigo", el Portal podrá requerir el nombre y correo electrónico de la persona a quien se le remita la información del inmueble. A través de esta funcionalidad,&nbsp;el Portal enviará al amigo un e-mail automático invitándolo a visitar cierto producto, utilizando el correo electrónico de la persona a quien se le remita únicamente para ese envío y posterior seguimiento. La información personal del amigo no será almacenada.</p>

<p style="text-align: justify;">El Usuario manifiesta que ha informado al tercero, destinatario del mensaje, sobre la utilización de sus datos personales, y que cuenta con el consentimiento del mismo para dicha utilización.</p>

<p style="text-align: justify;">En los envíos de las comunicaciones mencionadas, el Portal respetará las previsiones de las normas vigentes en materia de “spam”.</p>

<h5 style="text-align: justify;">9. Derechos de acceso, rectificación, cancelación y oposición de datos personales</h5>

<p style="text-align: justify;">REDIG pone en conocimiento del Usuario su derecho de acceso, actualización y cancelación de su información personal, los mismos que podrá ejercer mediante petición escrita a la siguiente dirección de correo electrónico:&nbsp;<a href="mailto:ayuda.usuarios@alquiledirecto.com">ayuda.usuarios@alquiledirecto.com</a></p>

<p style="text-align: justify;">Del mismo modo, el Usuario puede oponerse al uso o tratamiento de sus datos personales y puede solicitar ser informado sobre todas las finalidades con que se tratan los datos personales.</p>

<p style="text-align: justify;">Sin perjuicio de lo anterior, REDIG podrá conservar determinada información personal del Usuario que solicita la baja, a fin de que sirva de prueba ante una eventual reclamación contra REDIG por responsabilidades derivadas del tratamiento de dicha información. Dicha conservación deberá realizarse previo bloqueo de los datos, de manera que se impida su habitual tratamiento y su duración no podrá ser superior al plazo de prescripción legal de dichas responsabilidades.</p>

<h5 style="text-align: justify;">10. Modificaciones de las Políticas de Privacidad</h5>

<p style="text-align: justify;">REDIG se reserva expresamente el derecho a modificar, actualizar o completar en cualquier momento la presente Política de Privacidad.</p>

<p style="text-align: justify;">Cualquier modificación, actualización o ampliación producida en la presente Política de privacidad será inmediatamente publicada en el Portal, por lo que se recomienda al Usuario revisarla periódicamente, especialmente antes de proporcionar información personal.</p>

            </div>

            </div>

            </div>
        </div>
    </div>
@endsection
