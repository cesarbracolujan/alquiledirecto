<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="multikart">
    <meta name="keywords" content="multikart">
    <meta name="author" content="multikart">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <title>AlquileDirecto</title>

    <!--Google font-->
    {{-- <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital@1&display=swap" rel="stylesheet">

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/vendors/fontawesome.css') }}">

    <!--Slick slider css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/vendors/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/vendors/slick-theme.css') }}">

    <!-- Animate icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/vendors/animate.css') }}">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/vendors/themify-icons.css') }}">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/vendors/bootstrap.css') }}">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/vendors/themify-icons.css') }}">

    <!-- Theme css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/style.css') }}" media="screen" id="color">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@voerro/vue-tagsinput@2.6.0/dist/style.css">


</head>

<body class="theme-color-11">

    @include('layouts.partials.header')
    <div id="app">
        @yield('content')
    </div>

    @include('layouts.partials.footer')

    <!-- tap to top start -->
    <div class="tap-top">
        <div><i class="fa fa-angle-double-up"></i></div>
    </div>
    <!-- tap to top end -->



    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/app.js"></script>
    <script src="https://kit.fontawesome.com/016e86b3ac.js" crossorigin="anonymous"></script>

    <script src="{{ asset('multikart/assets/js/jquery-3.3.1.min.js') }}"></script>

    <!-- fly cart ui jquery-->
    <script src="{{ asset('multikart/assets/js/jquery-ui.min.js') }}"></script>

    <!-- exitintent jquery-->
    <script src="{{ asset('multikart/assets/js/jquery.exitintent.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/exit.js') }}"></script>

    <!-- popper js-->
    <script src="{{ asset('multikart/assets/js/popper.min.js') }}"></script>

    <!-- slick js-->
    <script src="{{ asset('multikart/assets/js/slick.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/slick-animation.min.js') }}"></script>

    <!-- menu js-->
    <script src="{{ asset('multikart/assets/js/menu.js') }}"></script>

    <!-- lazyload js-->
    <script src="{{ asset('multikart/assets/js/lazysizes.min.js') }}"></script>

    <!-- Bootstrap js-->
    <script src="{{ asset('multikart/assets/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Fly cart js-->
    <script src="{{ asset('multikart/assets/js/fly-cart.js') }}"></script>

    <!-- Theme js-->
    <script src="{{ asset('multikart/assets/js/script.js') }}" defer></script>
    <script src="{{ asset('multikart/assets/js/custom-slick-animated.js') }}"></script>
    <script>
        function openSearch() {
            document.getElementById("search-overlay").style.display = "block";
        }
        function closeSearch() {
            document.getElementById("search-overlay").style.display = "none";
        }
        function regresar(){
            window.history.back();
        }
    </script>




</body>
