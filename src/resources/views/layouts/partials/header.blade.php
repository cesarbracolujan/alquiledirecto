<header>
        <div class="mobile-fix-option"></div>
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="header-contact">
                            <ul>
                                <li>Bienvenidos a Alquile Directo</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i>Contactanos: <a href="https://wa.me/51990423797" target="_blank">+51 990-423-797</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 text-end">
                        <ul class="header-dropdown">
                            <li class="mobile-wishlist">
                                <a href="{{ route('profile.favorite') }}"><i class="fa fa-heart" aria-hidden="true"></i> Mis Favoritos</a>
                            </li>
                            @guest
                            <li class="onhover-dropdown mobile-account"> <i class="fa fa-user" aria-hidden="true"></i>
                                Mi Cuenta
                                <ul class="onhover-show-div">
                                    <li><a href="{{ route('login') }}" data-lng="en">Iniciar Sesión</a></li>
                                </ul>
                            </li>
                            @else
                            <li class="onhover-dropdown mobile-account"> <i class="fa fa-user" aria-hidden="true"></i>
                                {{ Auth::user()->firstname }}
                                <ul class="onhover-show-div">
                                    <li><a href="{{ route('profile.edit') }}">Editar Perfil</a></li>
                                    <li><a href="{{ route('profile.myads') }}">Mis Anuncios</a></li>
                                    <li><a href="{{ route('profile.favorite') }}">Mis Favoritos</a></li>
                                </ul>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-menu">
                        <div class="menu-left">
                            <div class="navbar">
                                <a href="javascript:void(0)" onclick="openNav()">
                                    <div class="bar-style"><i class="fa fa-bars sidebar-bar" aria-hidden="true"></i>
                                    </div>
                                </a>
                                <div id="mySidenav" class="sidenav">
                                    <a href="javascript:void(0)" class="sidebar-overlay" onclick="closeNav()"></a>
                                    <nav>
                                        <div onclick="closeNav()">
                                            <div class="sidebar-back text-left"><i class="fa fa-angle-left pr-2"
                                                    aria-hidden="true"></i> Regresar</div>
                                        </div>
                                        <ul id="sub-menu" class="sm pixelstrap sm-vertical">
                                            <li><a href="/ads">Buscar ...</a></li>
                                            <li> <a href="#">Mi Cuenta</a>
                                                <ul>
                                                    <li><a href="#">Editar Perfil</a></li>
                                                    <li><a href="#">Mis Anuncios</a></li>
                                                    <li><a href="#">Mis FAvoritos</a></li>
                                                </ul>
                                            </li>
                                            <li> <a href="#">Nosotros</a>
                                                <ul>
                                                    <li><a href="/nosotros">¿Quiénes Somos?</a></li>
                                                    <li><a href="/adquiriente">Para el Anunciante</a></li>
                                                    <li><a href="/anunciante">Para el Adquiriente</a></li>
                                                    <li><a href="/preguntas-frecuentes">Preguntas Frecuentes</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="brand-logo">
                                <a href="/"><img src="{{ asset('images/logos/logo-alquiledirecto.png') }}"
                                        class="img-fluid blur-up lazyload" alt=""></a>
                            </div>
                        </div>
                        <div class="menu-right pull-right">
                            <div>
                                <nav id="main-nav">
                                    <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                                    <ul id="main-menu" class="sm pixelstrap sm-horizontal">
                                        <li>
                                            <div class="mobile-back text-right">Regresar<i class="fa fa-angle-right pl-2"
                                                    aria-hidden="true"></i></div>
                                        </li>
                                        {{-- <li><a href="{{ route('payment.index')}}">Pagos</a></li> --}}
                                        <li><a href="{{ route('ads.index')}}">Anuncios Publicados</a></li>
                                        <li><a href="{{ route('ads.create')}}">Anuncia Gratis<div class="lable-nav">En solo 5 minutos</div></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</header>
