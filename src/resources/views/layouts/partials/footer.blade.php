<footer class="footer-light">
        <section class="section-b-space light-layout">
            <div class="container">
                <div class="row footer-theme partition-f">
                    <div class="col-lg-4 col-md-6">
                        <div class="footer-title footer-mobile-title">
                            <h4>Acerca de</h4>
                        </div>
                        <div class="footer-contant">
                            <div class="footer-logo"><img src="{{ asset('images/logos/logo-anunciante-popup.png') }}" alt=""></div>
                            <p class="text-justify">Alquile Directo es el primer portal web especializado en anuncios de alquiler de inmuebles, en donde encontraras en alquiler casa de ciudad, casa de playa, casa de campo, departamento de ciudad o playa, oficina, local industrial, local comercial, local para eventos, terreno, cochera para vehículo, habitacion y mucho más.</p>
                            <div class="footer-social">
                                <ul>
                                    <li><a href="https://www.facebook.com/alquiledirectoad/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="https://www.instagram.com/alquile_directo/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col offset-xl-1">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>Mi Cuenta</h4>
                            </div>
                            <div class="footer-contant">
                                <ul>
                                    @guest
                                    <li><a href="{{ route('login') }}"> Iniciar Sesión</a></li>
                                    @else
                                    <li><a href="{{ route('profile.edit') }}">Editar Perfil</a></li>
                                    <li><a href="{{ route('profile.myads') }}">Mis Anuncios</a></li>
                                    <li><a href="{{ route('profile.favorite') }}">Mis Favoritos</a></li>
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-lng="es">Cerrar Sesión</a></li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

                                    @endguest
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>Nosotros</h4>
                            </div>
                            <div class="footer-contant">
                                <ul>
                                    <li><a href="/nosotros">¿Quiénes Somos?</a></li>
                                    <li><a href="/adquiriente">Para el Anunciante</a></li>
                                    <li><a href="/anunciante">Para el Adquiriente</a></li>
                                    <li><a href="/preguntas-frecuentes">Preguntas Frecuentes (FAQ's)</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="sub-title">
                            <div class="footer-title">
                                <h4>Información</h4>
                            </div>
                            <div class="footer-contant">
                                <ul class="contact-list">
                                    <li><i class="fa fa-map-marker"></i>Alquile Directo</li>
                                    <li><i class="fa fa-phone"></i>Teléfono de contacto: <a href="https://wa.me/51990423797" target="_blank">990-423-797</a></li>
                                    <li><i class="fa fa-envelope-o"></i>Contáctanos: <a href="mailto:sistemas@trmansgementsac.com">sistemas@trmansgementsac.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-12">
                        <div class="footer-end">
                            <p><i class="fa fa-copyright" aria-hidden="true"></i> Copyright 2017. Todos los derechos reservados Lima Perú</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-12">
                        <div class="payment-card-bottom">
                            <ul>
                                <li>
                                    <a href="/politicas">Politicas de privacidad</a>
                                </li>
                                <li>
                                <a href="/terminos">Termninos y condiciones</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
