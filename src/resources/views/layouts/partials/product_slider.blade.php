<section class="section-b-space p-t-0 ratio_asos">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="product-4 product-m no-arrow">
                @foreach ($ads as $item)
                    <div class="product-box">
                        <div class="img-wrapper">
                            <div class="front">
                                <a href="/ads/show/{{ $item->id }}"><img src="{{ $item->image }}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="back">
                                <a href="/ads/show/{{ $item->id }}"><img src="{{ $item->gallery }}"
                                        class="img-fluid blur-up lazyload bg-img" alt=""></a>
                            </div>
                            <div class="cart-info cart-wrap">
                                <a href="javascript:void(0)" title="Agregar a Favoritos">
                                    <i class="ti-heart" aria-hidden="true"></i>
                                </a>
                                <a href="#" data-toggle="modal" data-target="#quick-view" title="Quick View">
                                    <i class="ti-search" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <div class="rating mb-2">
                                @for ($i = 1; $i < 6; $i++)
                                    @if ($i < $item->rating_number)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    @else
                                        @if ($item->rating_number > 0)
                                            @if (($item->rating_number - intval($item->rating_number)) != 0)
                                                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                            @else
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            @endif
                                        @else
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        @endif
                                    @endif
                                @endfor
                            </div>
                            <div class="mb-2">
                                <a href="/ads/show/{{ $item->id }}">
                                    <h6>{{ $item->name }}</h6>
                                </a>
                            </div>
                            @if ($item->ad_currency == '1')
                                <h4>$ {{ number_format($item->ad_price_dolar,2) }}</h4>
                            @else
                                <h4>S/ {{ number_format($item->ad_price_pen,2) }}</h4>
                            @endif

                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

