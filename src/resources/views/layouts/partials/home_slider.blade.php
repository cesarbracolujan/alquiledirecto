<section class="p-0">
    <div class="home-slider slider-animate"> <!-- slide-1 !-->
        <div>
            <div class="home text-center">
                <img src="{{ asset('images/home_slider_1.jpg') }}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain">
                                <div style="width: 100%;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="home">
                <img src="{{ asset('images/home_slider_2.jpg') }}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain text-dark">
                                <div style="width: 100%;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="home">
                <img src="{{ asset('images/home_slider_3.jpg') }}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain">
                                <div style="width: 100%;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- search section start -->
<section class="bg_cls section-b-space">
    <div class="search-section absolute-banner">
        <search-bar-ads />
    </div>
</section>
<!-- search section end -->
