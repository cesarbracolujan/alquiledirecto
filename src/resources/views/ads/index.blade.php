@extends('layouts.app')

@section('content')
    <ads-index :property_type_id="{{ json_encode((int)$property_type_id) }}" :geo_distric_id="{{ json_encode((int)$geo_distric_id) }}"></ads-index>
@endsection
