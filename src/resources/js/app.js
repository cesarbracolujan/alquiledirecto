/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import VueRx from 'vue-rx'
import Axios from 'axios'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import VoerroTagsInput from '@voerro/vue-tagsinput';
import VuejsClipper from "vuejs-clipper/dist/vuejs-clipper.umd";
import "vuejs-clipper/dist/vuejs-clipper.css";
import UUID from "vue-uuid";

import lang from 'element-ui/lib/locale/lang/es';
import locale from 'element-ui/lib/locale';
locale.use(lang);

import * as VueGoogleMaps from 'vue2-google-maps';
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBsmesPVos1z1-Qxlm3NRtQCsrXK2Trqeg',
        libraries: 'places'
    }
});

Vue.use(ElementUI, { size: 'medium' })
Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios;
Vue.config.keyCodes.backspace = 8;

Vue.use(VueRx)
Vue.use(VuejsClipper);
Vue.use(UUID);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('tags-input', VoerroTagsInput);

Vue.component('ads-index', require('./views/ads/index.vue').default);
Vue.component('ads-show', require('./views/ads/show.vue').default);
Vue.component('ads-form', require('./views/ads/form.vue').default);

Vue.component('payment-index', require('./views/payment/index.vue').default);

Vue.component('search-bar-ads', require('./components/SearchBarComponent.vue').default);

//profile
Vue.component('ads-favorite', require('./views/profile/favorite.vue').default);
Vue.component('ads-our', require('./views/profile/ourads.vue').default);


/* moment */
import moment from 'moment'
import 'moment/locale/es';
Vue.prototype.moment = moment
Vue.use(require('vue-moment'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: { "tags-input": VoerroTagsInput },
});
