<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Posts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('name', 120);
            $table->string('slug', 120);
            $table->string('description',400);
            $table->text('content')->nullable();
            $table->integer('orderby');
            $table->integer('status');
            $table->tinyInteger('estado');
            $table->float('rating_number',10,1);
            $table->integer('rating_comments');
            $table->integer('likes_number');
            $table->double('map_lat')->nullable();
            $table->double('map_lng')->nullable();
            $table->double('map_address')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->tinyInteger('feature');
            $table->string('image',255)->nullable();
            $table->string('primary_image',255);
            $table->integer('views');
            $table->timestamps(0);
            $table->softDeletes('deleted_at', 0);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
