<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaStorages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_storages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('name',255);
            $table->bigInteger('folder_id')->unsigned();
            $table->string('mime_type',120);
            $table->string('type',120);
            $table->integer('size');
            $table->string('public_url',255);
            $table->string('bucket',120);
            $table->string('bucket_key',120);
            $table->string('reference_type',120);
            $table->string('reference_id',255);
            $table->timestamps(0);
            $table->softDeletes('deleted_at',0);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_storages');
    }
}
