<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Companies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('contact_name',50);
            $table->string('document_type',3);
            $table->string('document_number',50)->unique();
            $table->string('social_reason',500);
            $table->string('telephone',50);
            $table->string('email',50)->unique();
            $table->string('address',300);
            $table->string('district',100);
            $table->string('department',100);
            $table->string('country',50);
            $table->char('valid_faq',1);
            $table->timestamps(0);
            $table->softDeletes('deleted_at', 0);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}