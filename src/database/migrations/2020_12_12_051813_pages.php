<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('name', 120);
            $table->string('slug', 120);
            $table->text('content');
            $table->integer('orderby');
            $table->tinyInteger('status');
            $table->bigInteger('user_id')->unsigned();
            $table->string('image', 255);
            $table->string('template', 60);          
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->integer('parent_id');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
