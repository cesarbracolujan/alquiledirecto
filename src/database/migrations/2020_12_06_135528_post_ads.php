<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PostAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_ads', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id')->unsigned();
            $table->bigInteger('post_anuncio_transaccion_id')->unsigned();
            $table->bigInteger('post_anuncio_tipo_inmueble_id')->unsigned();
            $table->integer('geo_department_id');
            $table->integer('geo_provinc_id');
            $table->integer('geo_distric_id');
            $table->string('geo_address',200)->nullable();
            $table->tinyInteger('ad_money');
            $table->double('ad_price_pen');
            $table->double('ad_price_dolar');
            $table->double('type_group_services');
            $table->text('basic_services');
            $table->text('aditional_services');
            $table->text('main_services');
            $table->tinyInteger('room_services');
            $table->tinyInteger('bathroom_services');
            $table->tinyInteger('kitchen_services');
            $table->tinyInteger('parking_available');
            $table->tinyInteger('main_bathroom_services');
            $table->string('total_area_services',200);
            $table->string('roofed_area_services',200);
            $table->tinyInteger('pool_services');
            $table->tinyInteger('furnished_services');
            $table->tinyInteger('allow_childrens');
            $table->tinyInteger('allow_pets');
            $table->tinyInteger('property_age');
            $table->text('location_description');
            $table->text('near_store');
            $table->text('near_park');
            $table->text('near_clinic');
            $table->text('near_school');
            $table->text('near_library');
            $table->text('near_bank');
            $table->text('near_vet');
            $table->text('principal_city');
            $table->text('type_services');
            $table->text('other_services');
            $table->text('desc_service_1');
            $table->text('desc_service_2');
            $table->text('discount_price_1');
            $table->text('discount_price_2');
            $table->text('discount_price_3');
            $table->text('comments');
            $table->timestamps(0);
            $table->softDeletes('deleted_at', 0);

            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('post_anuncio_transaccion_id')->references('id')->on('post_ad_transactions');
            $table->foreign('post_anuncio_tipo_inmueble_id')->references('id')->on('post_ad_property_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_ads');
    }
}
