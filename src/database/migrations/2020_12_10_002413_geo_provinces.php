<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GeoProvinces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_provinces', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->bigInteger('geo_departments_id')->unsigned();
            $table->timestamps();

            $table->foreign('geo_departments_id')->references('id')->on('geo_departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_provinces');
    }
}
