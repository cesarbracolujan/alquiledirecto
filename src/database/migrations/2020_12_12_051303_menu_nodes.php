<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MenuNodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_nodes', function (Blueprint $table) {
            $table->id();
            $table->integer('menu_content_id');
            $table->integer('parent_id');
            $table->integer('related_id');
            $table->string('type',200);
            $table->string('url',200);
            $table->string('icon_front',255);
            $table->integer('position');
            $table->string('title',200);
            $table->string('css_class',200);
            $table->timestamps(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_nodes');
    }
}
