<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WidgetSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_settings', function (Blueprint $table) {
            $table->id();
            $table->string('name',120);
            $table->text('configuration_data');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('widget_id')->unsigned();
            $table->bigInteger('context_id')->unsigned();
            $table->tinyInteger('order');
            $table->timestamps(0);
            $table->softDeletes('deleted_at',0);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('widget_id')->references('id')->on('widgets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget_settings');
    }
}
