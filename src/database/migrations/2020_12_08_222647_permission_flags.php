<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PermissionFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_flags', function (Blueprint $table) {
            $table->id();            
            $table->string('flag',100);
            $table->string('name',100);
            $table->integer('parent_flag');
            $table->string('context',10);
            $table->integer('is_feature');
            $table->integer('feature_visible');
            $table->integer('permission_visible');
            $table->timestamps(0);
            $table->softDeletes('deleted_at', 0);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_flags');
    }
}
