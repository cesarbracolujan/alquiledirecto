<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name', 120);
            $table->string('slug', 120);
            $table->integer('parent_id');
            $table->text('description');
            $table->integer('status');
            $table->bigInteger('user_id')->unsigned();
            $table->integer('order_by');
            $table->string('icon', 120)->nullable();
            $table->timestamps(0);
            $table->softDeletes('deleted_at', 0);

            $table->foreign('user_id')->references('id')->on('users');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
