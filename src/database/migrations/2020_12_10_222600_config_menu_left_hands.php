<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConfigMenuLeftHands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_menu_left_hands', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id')->nullable();
            $table->integer('lft');
            $table->integer('rgt');
            $table->integer('depth');
            $table->string('kind',20);
            $table->string('default_name',255);
            $table->string('name',255);
            $table->integer('feature_id')->nullable();
            $table->string('icon',50)->nullable();
            $table->bigInteger('context_id')->unsigned();
            $table->timestamps(0);
            $table->softDeletes('deleted_at',0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_menu_left_hands');
    }
}
