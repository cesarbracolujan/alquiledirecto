<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_uploads', function (Blueprint $table) {
            $table->id();
            $table->string('name',511);
            $table->string('cloud_name',511);
            $table->string('mime_type',127);
            $table->integer('size');
            $table->string('public_url',511);
            $table->text('description');
            $table->string('reference_id',255);
            $table->enum('reference_type',['', 'users', 'posts', 'sliders']);
            $table->integer('uploaded_by');
            $table->timestamps(0);
            $table->softDeletes('deleted_at',0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_uploads');
    }
}
