<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AuditHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_histories', function (Blueprint $table) {
            $table->id();            
            $table->string('module',60);
            $table->text('request');
            $table->enum('action',['loggedin', 'loggedout', 'password', 'created', 'updated', 'deleted', 'restored', 'read']);
            $table->string('user_agent',255);
            $table->string('ip_address',25);
            $table->integer('reference_user');
            $table->integer('reference_id');
            $table->integer('context_id');
            $table->timestamps(0);
            $table->softDeletes('deleted_at', 0);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_histories');
    }
}
