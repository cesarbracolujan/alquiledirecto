<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TeamFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_features', function (Blueprint $table) {
            $table->bigInteger('team_id')->unsigned();
            $table->bigInteger('feature_id')->unsigned();
            $table->timestamps(0);
            $table->softDeletes('deleted_at',0);

            $table->foreign('team_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_features');
    }
}
