<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GeoDistricts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_districts', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('slug',120);
            $table->bigInteger('geo_province_id')->unsigned();
            $table->timestamps(0);

            $table->foreign('geo_province_id')->references('id')->on('geo_provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_districts');
    }
}
