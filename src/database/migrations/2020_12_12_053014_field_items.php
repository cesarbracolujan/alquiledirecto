<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FieldItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_items', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('field_group_id')->unsigned();
            $table->integer('parent_id');
            $table->tinyInteger('position');
            $table->string('title',120);
            $table->string('slug',120);
            $table->string('field_type',120);
            $table->text('instructions');
            $table->text('options');
            $table->timestamps(0);
            $table->softDeletes('deleted_at',0);

            $table->foreign('field_group_id')->references('id')->on('field_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_items');
    }
}
