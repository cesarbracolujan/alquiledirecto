<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name',120);
            $table->text('permissions');
            $table->string('description',255);
            $table->tinyInteger('is_staff');
            $table->string('reference',10);
            $table->integer('reference_id');
            $table->string('context',10);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps(0);
            $table->softDeletes('deleted_at',0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
