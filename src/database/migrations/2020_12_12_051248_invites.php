<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Invites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites', function (Blueprint $table) {
            $table->id();
            $table->string('token',128);
            $table->tinyInteger('accepted');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('invitee_id')->unsigned();
            $table->bigInteger('reference_id')->unsigned();
            $table->string('reference_type',10);
            $table->bigInteger('role_id')->unsigned();
            $table->timestamps(0);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invites');
    }
}
