<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PropertyTypeFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_type_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order');
            $table->bigInteger('post_ad_property_type_id')->unsigned();
            $table->bigInteger('post_ad_feature_id')->unsigned();
            $table->timestamps(0);

            $table->foreign('post_ad_property_type_id')->references('id')->on('post_ad_property_types');
            $table->foreign('post_ad_feature_id')->references('id')->on('post_ad_features');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_type_features');
    }
}
