<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PostFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_features', function (Blueprint $table) {
            $table->id();
            $table->integer('order');
            $table->bigInteger('post_id')->unsigned();
            $table->bigInteger('post_ad_feature_id')->unsigned();
            $table->timestamps(0);
            

            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('post_ad_feature_id')->references('id')->on('post_ad_features');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_features');
    }
}
