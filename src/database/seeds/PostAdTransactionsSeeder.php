<?php

use App\Imports\PostAdTransactionsImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class PostAdTransactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new PostAdTransactionsImport, public_path('data/data_post_ad_transactions.xlsx'));
    }
}
