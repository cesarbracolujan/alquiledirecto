<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FieldGroupsSeeder extends Seeder {

    public function run()
    {
        DB::table('field_groups')->delete();

        DB::table('field_groups')->insert(array('title' => 'Search Engine Optimization','field_rules' => '[{\"field_relation\":\"and\",\"field_options\":[{\"rel_name\":\"model_name\",\"rel_value\":\"Page\",\"rel_type\":\"==\"}]}]','position' => 0,'status' => 1));
        DB::table('field_groups')->insert(array('title' => 'campos Inicio','field_rules' => '[{\"field_relation\":\"and\",\"field_options\":[{\"rel_name\":\"post_template\",\"rel_value\":\"template_home\",\"rel_type\":\"==\"}]}]','position' => 0,'status' => 1));
        DB::table('field_groups')->insert(array('title' => 'Plantilla Nosotros','field_rules' => '[{\"field_relation\":\"and\",\"field_options\":[{\"rel_name\":\"post_template\",\"rel_value\":\"template_nosotros\",\"rel_type\":\"==\"}]}]','position' => 0,'status' => 1));
        DB::table('field_groups')->insert(array('title' => 'campos Anuncios','field_rules' => '[{\"field_relation\":\"and\",\"field_options\":[{\"rel_name\":\"model_name\",\"rel_value\":\"Post\",\"rel_type\":\"==\"}]}]','position' => 0,'status' => 1));

    }

}
