<?php

use App\Imports\FeaturesImport;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class FeaturesSeeder extends Seeder {

    public function run()
    {
        Excel::import(new FeaturesImport, public_path('data/data_features.xlsx'));
    }

}
