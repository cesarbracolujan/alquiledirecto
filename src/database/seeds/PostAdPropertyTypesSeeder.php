<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PostAdPropertyTypeImport;


class PostAdPropertyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new PostAdPropertyTypeImport, public_path('data/data_post_ad_property_types.xlsx'));
    }
}
