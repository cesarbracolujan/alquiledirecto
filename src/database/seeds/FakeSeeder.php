<?php

use App\Imports\PostAdsImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use App\Imports\PostsImport;
use App\Imports\PostFeaturesImport;
use App\Imports\PropertyTypefeaturesImport;
use App\Imports\PostAdGalleriesImport;

class FakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Excel::import(new UsersImport, public_path('data/data_users.xlsx'));
        // Excel::import(new PostsImport, public_path('data/data_posts.xlsx'));
        //Excel::import(new PostFeaturesImport, public_path('data/data_post_features.xlsx'));
        // Excel::import(new PostAdsImport, public_path('data/data_post_ads.xlsx'));
        //Excel::import(new PropertyTypefeaturesImport, public_path('data/data_property_type_features.xlsx'));
        // Excel::import(new PostAdGalleriesImport, public_path('data/data_post_ad_galleries.xlsx'));

    }
}
