<?php


use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PostAdFeaturesImport;


class PostAdFeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new PostAdFeaturesImport, public_path('data/data_post_ad_features.xlsx'));
    }
}
