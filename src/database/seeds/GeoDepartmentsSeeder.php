<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeoDepartmentsSeeder extends Seeder {

    public function run()
    {
        DB::table('geo_departments')->delete();

        DB::table('geo_departments')->insert(array('name' => 'AMAZONAS'));
        DB::table('geo_departments')->insert(array('name' => 'ANCASH'));
        DB::table('geo_departments')->insert(array('name' => 'APURIMAC'));
        DB::table('geo_departments')->insert(array('name' => 'AREQUIPA'));
        DB::table('geo_departments')->insert(array('name' => 'AYACUCHO'));
        DB::table('geo_departments')->insert(array('name' => 'CAJAMARCA'));
        DB::table('geo_departments')->insert(array('name' => 'CALLAO'));
        DB::table('geo_departments')->insert(array('name' => 'CUSCO'));
        DB::table('geo_departments')->insert(array('name' => 'HUANCAVELICA'));
        DB::table('geo_departments')->insert(array('name' => 'HUANUCO'));
        DB::table('geo_departments')->insert(array('name' => 'ICA'));
        DB::table('geo_departments')->insert(array('name' => 'JUNIN'));
        DB::table('geo_departments')->insert(array('name' => 'LA LIBERTAD'));
        DB::table('geo_departments')->insert(array('name' => 'LAMBAYEQUE'));
        DB::table('geo_departments')->insert(array('name' => 'LIMA'));
        DB::table('geo_departments')->insert(array('name' => 'LORETO'));
        DB::table('geo_departments')->insert(array('name' => 'MADRE DE DIOS'));
        DB::table('geo_departments')->insert(array('name' => 'MOQUEGUA'));
        DB::table('geo_departments')->insert(array('name' => 'PASCO'));
        DB::table('geo_departments')->insert(array('name' => 'PIURA'));
        DB::table('geo_departments')->insert(array('name' => 'PUNO'));
        DB::table('geo_departments')->insert(array('name' => 'SAN MARTIN'));
        DB::table('geo_departments')->insert(array('name' => 'TACNA'));
        DB::table('geo_departments')->insert(array('name' => 'TUMBES'));
        DB::table('geo_departments')->insert(array('name' => 'UCAYALI'));


    }

}
