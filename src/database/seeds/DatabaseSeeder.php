<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // ConfigMenuLeftHandsSeeder::class,
            FieldGroupsSeeder::class,
            FieldItemsSeeder::class,
            GeoCountriesSeeder::class,
            GeoDepartmentsSeeder::class,
            GeoProvincesSeeder::class,
            GeoDistrictsSeeder::class,
            PostAdPropertyTypesSeeder::class,
            PostAdFeaturesSeeder::class,
            PostAdTransactionsSeeder::class,
            SettingsSeeder::class,
            FeaturesSeeder::class
        ]);

        $this->call([
            FakeSeeder::class
        ]);

    }
}
