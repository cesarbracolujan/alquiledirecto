<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('cmsadmin')->prefix('cms')->group(function() {
    Route::get('/', 'CmsController@index')->name('cms.dashboard');

    Route::prefix('configuration-web')->group(function() {
        Route::get('', 'ConfigurationWebController@index')->name('cms.congifuration.web');
        Route::get('record/{type}', 'ConfigurationWebController@record');
        Route::post('record/{type}', 'ConfigurationWebController@store');
    });

    Route::prefix('property-type')->group(function() {
        Route::get('', 'PropertyTypeController@index')->name('cms.property-type');
        Route::get('records', 'PropertyTypeController@records');
        Route::get('record/{id}', 'PropertyTypeController@record');
        Route::post('', 'PropertyTypeController@store');
        Route::delete('{id}', 'PropertyTypeController@destroy');

    });
});



