<?php

namespace Modules\Cms\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyTypeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'name' => [
                'required',
            ],
            'type_data' => [
                'required',
            ],
        ];
    }

    public function attributes()
    {
        return [
            'type_data' => 'tipo dato',
            'name' => 'nombre',

        ];
    }
}
