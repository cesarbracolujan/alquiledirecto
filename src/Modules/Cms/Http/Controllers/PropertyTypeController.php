<?php

namespace Modules\Cms\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cms\Http\Resources\PropertyTypeCollection;
use Modules\Cms\Http\Resources\PropertyTypeResource;
use Modules\Cms\Http\Requests\PropertyTypeRequest;
use App\Models\CatPostAdPropertyType;
use Exception;


class PropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('cms::property-type.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        //return view('cms::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(PropertyTypeRequest $request)
    {
        $id = $request->input('id');
        $row = CatPostAdPropertyType::firstOrNew(['id' => $id]);
        $row->fill($request->all());
        $row->save();

        return [
            'success' => true,
            'message' => ($id) ? 'Registro editado con éxito':'Rgistrado con éxito'
        ];
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        //return view('cms::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        //return view('cms::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {

        try {

            $record = CatPostAdPropertyType::findOrFail($id);
            $record->delete();

            return [
                'success' => true,
                'message' => 'Registro eliminado con éxito'
            ];

        } catch (Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => 'El registro esta siendo usado por otros registros, no puede eliminar'] : ['success' => false,'message' => 'Error inesperado, no se pudo eliminar el registro'];

        }
    }

    public function records() {
        return new PropertyTypeCollection(CatPostAdPropertyType::latest()->paginate(\Config::get('cms.items_per_page')));
    }

    public function record($id) {
        return new PropertyTypeResource(CatPostAdPropertyType::findOrFail($id));
    }
}
