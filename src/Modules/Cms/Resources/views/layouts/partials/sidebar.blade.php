<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper"><a href="index.html"><img class="blur-up lazyloaded" src="{{ asset('multikart/assets/images/dashboard/multikart-logo.png') }}" alt=""></a></div>
    </div>
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle lazyloaded blur-up" src="{{ asset('multikart/assets/images/dashboard/man.png') }}" alt="#">
            </div>
            <h6 class="mt-3 f-14">JOHN</h6>
            <p>general manager.</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{ route('cms.dashboard') }}"><i data-feather="home"></i><span>Dashboard</span></a></li>
            <li><a class="sidebar-header" href="{{ route('cms.congifuration.web') }}"><i data-feather="home"></i><span>Configuración Web</span></a></li>
            <li><a class="sidebar-header" href="{{ route('cms.property-type') }}"><i data-feather="home"></i><span>Tipo Inmueble</span></a></li>

            {{-- <li><a class="sidebar-header" href=""><i data-feather="box"></i><span>Productos</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="order.html"><i class="fa fa-circle"></i>Lista</a></li>
                    <li><a href="transactions.html"><i class="fa fa-circle"></i>Nuevo</a></li>
                </ul>
            </li> --}}
        </ul>
    </div>
</div>
