<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Multikart admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Multikart admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ asset('multikart/assets/images/dashboard/favicon.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('multikart/assets/images/dashboard/favicon.png') }}" type="image/x-icon">
    <title>CMS</title>

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/fontawesome.css') }}">

    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/flag-icon.css') }}">

    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/icofont.css') }}">

    <!-- Prism css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/prism.css') }}">

    <!-- Chartist css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/chartist.css') }}">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/bootstrap.css') }}">

    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/admin.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('multikart/assets/css/custom.css') }}">

</head>

<body>

    <div class="page-wrapper">

        @include('cms::layouts.partials.header')

        <div class="page-body-wrapper">

            @include('cms::layouts.partials.sidebar')

            <div class="page-body" style="padding: 5px;" id="app">
                @yield('content')
            </div>

            @include('cms::layouts.partials.footer')
        </div>

    </div>

    <!-- latest jquery-->
    <script src="{{ asset('multikart/assets/js/jquery-3.3.1.min.js') }}"></script>

    <!-- Bootstrap js-->
    <script src="{{ asset('multikart/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/bootstrap.js') }}"></script>

    <!-- feather icon js-->
    <script src="{{ asset('multikart/assets/js/icons/feather-icon/feather.min.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/icons/feather-icon/feather-icon.js') }}"></script>

    <!-- Sidebar jquery-->
    <script src="{{ asset('multikart/assets/js/sidebar-menu.js') }}"></script>

    <!--chartist js
    <script src="{{ asset('multikart/assets/js/chart/chartist/chartist.js') }}"></script>-->

    <!--chartjs js
    <script src="{{ asset('multikart/assets/js/chart/chartjs/chart.min.js') }}"></script>-->

    <!-- lazyload js-->
    <script src="{{ asset('multikart/assets/js/lazysizes.min.js') }}"></script>

    <!--copycode js-->
    <script src="{{ asset('multikart/assets/js/prism/prism.min.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/clipboard/clipboard.min.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/custom-card/custom-card.js') }}"></script>

    <!--counter js-->
    <script src="{{ asset('multikart/assets/js/counter/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/counter/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('multikart/assets/js/counter/counter-custom.js') }}"></script>

    <!--peity chart js-->
    <script src="{{ asset('multikart/assets/js/chart/peity-chart/peity.jquery.js') }}"></script>

    <!--sparkline chart js-->
    <script src="{{ asset('multikart/assets/js/chart/sparkline/sparkline.js') }}"></script>

    <!--Customizer admin-->
    <script src="{{ asset('multikart/assets/js/admin-customizer.js') }}"></script>

    <!--dashboard custom js
    <script src="{{ asset('multikart/assets/js/dashboard/default.js') }}"></script>-->

    <!--right sidebar js-->
    <script src="{{ asset('multikart/assets/js/chat-menu.js') }}"></script>

    <!--height equal js-->
    <script src="{{ asset('multikart/assets/js/height-equal.js') }}"></script>

    <!-- lazyload js-->
    <script src="{{ asset('multikart/assets/js/lazysizes.min.js') }}"></script>

    <!--script admin-->
    <script src="{{ asset('multikart/assets/js/admin-script.js') }}"></script>

    <script src="{{ asset('js/cms.js') }}"></script>

    <script>
        feather.replace()
    </script>

    @stack('scripts')

</body>
