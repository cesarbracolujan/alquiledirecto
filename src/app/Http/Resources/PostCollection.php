<?php

namespace App\Http\Resources;

use App\Models\PostFavoriteUser;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'name' => $row->name,
                'slug' => $row->slug,
                'description' => $row->description,
                'content' => $row->content,
                'image' => self::pathImage($row->image),
                'primary_image' => self::pathImage($row->primary_image),
                'ad_currency' => $row->ads->ad_money,
                'ad_price_pen' => $row->ads->ad_price_pen,
                'ad_price_dolar' => $row->ads->ad_price_dolar,
                'rating_number' => $row->rating_number,
                'likes_number' => $row->likes_number,
                'geo_address' => $row->ads->geo_address,
                'is_favorite' => self::isFavorite($row->id),
                'gallery' => $row->galleries->transform(function($row_gallery){
                    return [
                        'id' => $row_gallery->id,
                        'url' => asset($row_gallery->image_resize),
                        'original' => asset($row_gallery->image),
                    ];
                }),
                'property_type' => $row->ads->propertyType->name,
                'transaction' => $row->ads->transaction->name,
                'created_date' => $row->created_at,
                'status' => $row->estado === 1 ? 'Activo' : 'Incompleto',
                'views' => $row->views,
            ];
        });
    }

    public static function pathImage($image) {
        \Log::info("image:", [$image]);
        $path = asset('uploads'.DIRECTORY_SEPARATOR.'default.jpg');
        $exists = Storage::disk('images')->exists($image);
        if($exists) {

            $path = asset($image);
        }
        return $path;
    }

    public static function isFavorite($post_id)
    {
        $record = PostFavoriteUser::where([
            ["post_id","=",$post_id],
            ["user_id","=",Auth::id()]
        ])->count();

        if($record > 0)
        {
            return true;
        }
        return false;
    }

    public static function getFirstGallery($gallery)
    {
        $array = $gallery->transform(function($row_gallery){
            return [
                'id' => $row_gallery->id,
                'url' => asset($row_gallery->image_resize),
            ];});

        return $array[0].url;
    }
}
