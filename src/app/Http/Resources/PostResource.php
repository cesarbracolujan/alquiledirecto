<?php

namespace App\Http\Resources;

use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'content' => $this->content,
            'rating_number' => $this->rating_number,
            'likes_number' => $this->likes_number,
            'map_lat' => $this->map_lat,
            'map_lng' => $this->map_lng,
            'address' => $this->ads->geo_address,
            'image' => self::pathImage($this->image),
            'primary_image' => self::pathImage($this->primary_image),
            'views' => $this->views,
            'ad_currency' => $this->ads->ad_money,
            'ad_price_pen' => $this->ads->ad_price_pen,
            'ad_price_dolar' => $this->ads->ad_price_dolar,
            'exchange_rate' => Setting::select('value')->where('key','=','config_tipo_cambio')->get('value')[0]->value,
            'gallery' => $this->galleries->transform(function($row){
                return [
                    'id' => $row->id,
                    'url' => asset($row->image),
                    'url_thumb' => asset($row->image_resize),
                ];
            }),
            'contact_name' => $this->user->contact_name,
            'contact_email' => $this->user->email,
            'contact_cel' => $this->user->phone,
            'property_type' => $this->ads->propertyType->name,
            'basic_features' => $this->ads->basic_services,
            'aditional_features' => $this->ads->aditional_services,
            'main_features' => $this->ads->main_services,

        ];
    }

    public static function pathImage($image) {
        $path = asset('uploads'.DIRECTORY_SEPARATOR.'default.jpg');
        $exists = Storage::disk('images')->exists($image);
        if($exists) {
            $path = asset($image);
        }
        return $path;
    }



}
