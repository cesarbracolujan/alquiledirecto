<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Post;
use Illuminate\Support\Facades\Storage;


class AdsTopComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('ads', Post::latest('id')->limit(6)->get()->transform(function($row) {
            return (object)[
                'id' => $row->id,
                'name' => $row->name,
                'slug' => $row->slug,
                'description' => $row->description,
                'content' => $row->content,
                'image' => self::pathImage($row->image),
                'primary_image' => self::pathImage($row->primary_image),
                'ad_currency' => $row->ads->ad_money,
                'ad_price_pen' => $row->ads->ad_price_pen,
                'ad_price_dolar' => $row->ads->ad_price_dolar,
                'rating_number' => $row->rating_number,
                'gallery' => self::getFirstGallery($row->galleries),
            ];
        }));

    }

    public static function pathImage($image) {
        $path = asset('uploads'.DIRECTORY_SEPARATOR.'default.jpg');
        $exists = Storage::disk('images')->exists($image);
        if($exists) {
            $path = asset($image);
        }
        return $path;
    }

    public static function getFirstGallery($gallery)
    {
        $array = $gallery->transform(function($row_gallery){
            return asset($row_gallery->image_resize);});

        return $array[0];
    }
}
