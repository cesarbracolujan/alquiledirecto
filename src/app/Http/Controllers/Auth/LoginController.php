<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\SocialProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($driver)
    {
        $drivers = ['facebook',  'google'];

        if(in_array($driver, $drivers)) {
            return Socialite::driver($driver)->redirect();
        }
        else {
            return redirect()->route('login')->with('info', $driver . 'no es una aplicación para poder loguearse.l');
        }

    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request, $driver)
    {
        if ($request->get('error')) {
            return redirect()->route('login');
        }

        $socialite = Socialite::driver($driver)->user();


        $social_profile = SocialProfile::where('social_id', $socialite->getId())->where('social_name', $driver)->first();

        if (!$social_profile) {

            $user = User::where('email', $socialite->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'firstname' => $driver === 'google' ? $socialite['given_name'] : $socialite->getName(),
                    'lastname' => $driver === 'google' ? $socialite['family_name'] : $socialite->getName(),
                    'email' => $socialite->getEmail(),
                    'password' => '',
                    'completed_profile' => 0,
                    'term_accepted' => 1,
                    'email_verified_at' => date('Y/m/d h:i:s'),
                ]);
            }

            $social_profile = SocialProfile::create([
                'user_id' => $user->id,
                'social_id' => $socialite->getId(),
                'social_name' => $driver,
                'social_avatar' => $socialite->getAvatar(),
            ]);
        }

        auth()->login($social_profile->user);
        return redirect('/');
    }
}
