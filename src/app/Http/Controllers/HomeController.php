<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CatPostAdPropertyType;
use App\Models\GeoDistrict;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function tables() {
        return [
            'property_types' => CatPostAdPropertyType::select('id', 'name')->get(),
            'districts' => GeoDistrict::select('id', 'name as value')->limit(10)->get()
        ];
    }

    public function remoteSearch(Request $request) {
        return [
            'data' => GeoDistrict::select('id', 'name as value')->where('name', 'like', "%{$request->search}%")->limit(10)->get()
        ];
    }
}
