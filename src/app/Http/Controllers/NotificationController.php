<?php

namespace App\Http\Controllers;

use App\Channels\WhatsAppChannel;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function sendQuestionToAuthor(Request $request)
    {
        $post_id = $request['id'];
        $post = Post::findOrFail($post_id);
        $user = User::findOrFail($post->user_id);

        $objQuestion = new \stdClass();
        $objQuestion->username = $user->firstname;
        $objQuestion->post = $post->name;
        $objQuestion->url = URL::to('/').'/ads/show/'.$post->id;
        $objQuestion->contact_email = $request['email'];
        $objQuestion->contact_phone = '+51'.$request['phone_number'];
        $objQuestion->contact_name = $request['name'];
        $objQuestion->message = $request['message'];

        // WhatsAppChannel::send($from,$to,$message);
        WhatsAppChannel::sentEmail($user->email,$objQuestion);

        return [
            'success' => true,
            'message' =>  'Mensaje Enviado.',
        ];
    }
}
