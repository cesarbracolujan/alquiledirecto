<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\CatPostAdPropertyType;
use App\Models\CatPostAdFeature;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use Illuminate\Database\Eloquent\Builder;
use App\Models\GeoDistrict;
use App\Helpers\UploadFileHelper;
use App\Http\Requests\PostRequest;
use App\Http\Traits\UploadImageTrait;
use App\Models\CatFeature;
use App\Models\CatPostAdTransaction;
use App\Models\GeoDepartment;
use App\Models\GeoProvince;
use App\Models\PostAd;
use App\Models\PostAdGallery;
use App\Models\PostFavoriteUser;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\PostAdCalendar;

class AdsController extends Controller
{
    use UploadImageTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $property_type_id = $request->property_type_id;
        $geo_distric_id = $request->geo_distric_id;

        return view('ads.index', compact('property_type_id', 'geo_distric_id'));
    }

    public function create() {
        return view('ads.form');
    }

    public function show($id)
    {
        return view('ads.show', compact('id'));
    }

    public function store(PostRequest $request) {
        return 'sd';
    }

    public function records(Request $request) {

        $records = Post::latest('id');
        $property_types_id = $request->property_types_id;
        $features_id = $request->features_id;
        $max_price = $request->max_price;
        $min_price = $request->min_price;
        $geo_distric_id = $request->geo_distric_id;

        if($property_types_id || $max_price || $min_price || $geo_distric_id ) {
            $records = $records->whereHas('ads', function (Builder $query) use ($property_types_id, $max_price, $min_price, $geo_distric_id){
                if($property_types_id) {
                    $query->whereIn('post_anuncio_tipo_inmueble_id', $property_types_id);
                }
                if($max_price) {
                    $query->where('ad_price_pen', '<=',(float)$max_price);
                }
                if($min_price) {
                    $query->where('ad_price_pen', '>=',(float)$min_price);
                }

                if($geo_distric_id) {
                    $query->where('geo_distric_id', $geo_distric_id);
                }
            });
        }

        if($features_id) {
            $records = $records->whereHas('features', function (Builder $query) use ($features_id){
                $query->whereIn('post_ad_feature_id', $features_id);
            });
        }

        return new PostCollection($records->paginate(env('ITEMS_PER_PAGE')));
    }

    public function record($id) {
        $post = Post::findOrFail($id);
        $post->views = $post->views + 1;
        $post->save();

        return new PostResource($post);
    }

    public function tables(Request $request) {

        $districts = GeoDistrict::select('id', 'name')->limit(10);
        if($request->distric_id) {
            $districts = $districts->where('id', $request->distric_id);
        }
        return [
            'property_types' => CatPostAdPropertyType::select('id', 'name')->get()->append('selected'),
            'features' => CatPostAdFeature::select('id', 'name')->get()->append('selected'),
            'transactions' => CatPostAdTransaction::select('id','name')->get()->append('selected'),
            'districts' => $districts->get(),
        ];
    }

    public function getDepartments(){
        $departments = GeoDepartment::select('id as value', 'name')->get();
        return [
            'departments' =>  $departments,
        ];
    }

    public function getProvinceByDepartmentId($id){
        $provinces = GeoProvince::select('id as value', 'name')->where('geo_departments_id', $id)->get()->append('selected');
        return [
            'provinces' => $provinces
        ];
    }
    public function getdistrictByProvinceId($id){
        $districts = GeoDistrict::select('id as value', 'name')->where('geo_province_id', $id)->get()->append('selected');
        return [
            'districts' => $districts
        ];
    }

    public function getFeaturesByTypeOfProperty($id){
        $basic_features = CatFeature::select()->where('feature_type','=','1')->get();
        $detail_features = CatFeature::select()->where('feature_type','=','2')->get();
        $other_features = CatFeature::select()->where('feature_type','=','3')->get();

        return [
            'basic_features' => $basic_features,
            'detail_features' => $detail_features,
            'other_features' => $other_features
        ];
    }

    public function remoteSearch(Request $request) {
        return [
            'districts' => GeoDistrict::select('id', 'name as value')->where('name', 'like', "%{$request->search}%")->limit(10)->get()
        ];
    }

    public function upload(Request $request)
    {
        $validate_upload = UploadFileHelper::validateUploadFile($request, 'file', 'jpg,jpeg,png,gif,svg');
        if(!$validate_upload['success']){
            return $validate_upload;
        }
        if ($request->hasFile('file')) {
            $new_request = [
                'file' => $request->file('file'),
                'type' => $request->input('type'),
                'hash_id' => $request['hash_id'],
            ];

            $type = $request['type'];

            if($type == 'primary_image')
                return $this->upload_primary_image($new_request);

            return $this->upload_image($new_request);
        }

        return [
            'success' => false,
            'message' =>  'Error al cargar archivo.',
        ];
    }

    function upload_primary_image($request)
    {
        $file = $request['file'];

        $hash_id = $request['hash_id'];
        $filename = $file->getClientOriginalName();

        $temp = tempnam(sys_get_temp_dir(), $hash_id);
        file_put_contents($temp, file_get_contents($file));
        $extension = \File::extension($filename);

        $uploaded = Storage::put('temp/'.$hash_id.'/primary_image.'.$extension, file_get_contents($file->getRealPath()), 'public');

        $mime = mime_content_type($temp);
        $data = file_get_contents($temp);

        return [
            'success' => true,
            'data' => [
                'filename' => $file->getClientOriginalName(),
                'temp_path' => $temp,
                'temp_image' => 'data:' . $mime . ';base64,' . base64_encode($data)
            ]
        ];
    }

    function upload_image($request)
    {
        $file = $request['file'];
        $hash_id = $request['hash_id'];
        $filename = $file->getClientOriginalName();

        $uploaded = Storage::put('temp/'.$hash_id.'/'.$filename, file_get_contents($file->getRealPath()), 'public');

        $temp = tempnam(sys_get_temp_dir(), $hash_id);
        file_put_contents($temp, file_get_contents($file));
        $data = file_get_contents($temp);
        $mime = mime_content_type($temp);

        return [
            'success' => true,
            'data' => [
                'filename' => $file->getClientOriginalName(),
                'temp_path' => $temp,
                'temp_image' => 'data:' . $mime . ';base64,' . base64_encode($data)
            ]
        ];
    }

    public function favoritePost($post)
    {
        PostFavoriteUser::create([
            'order' => 0,
            'post_id' => $post,
            'user_id' => Auth::id()
        ]);

        $post = Post::findOrFail($post);
        $post->likes_number = $post->likes_number + 1;
        $post->save();

        return [
            'success' => true,
            'message' =>  'Anuncion agregado a favoritos',
        ];
    }

    public function unFavoritePost($post)
    {

        $record = PostFavoriteUser::where([
            ["post_id","=",$post],
            ["user_id","=",Auth::id()]
        ]);

        $post = Post::findOrFail($post);
        $post->likes_number = $post->likes_number === 0 ? 0 : $post->likes_number - 1;
        $post->save();

        $record->delete();

        return [
            'success' => true,
            'message' =>  'Anuncio quitado de favoritos',
        ];
    }

    public function saveAd(Request $request)
    {
        $hash_id = $request['hash_id'];
        $validated = $request->validate([
            'name' => 'required',
        ]);
        \Log::info('$request' , [$request]);

        // if($validated)
        // {
        //     \Log::info('errores' , [$validated]);
        //     return $validated;
        // }

        try {
            \DB::beginTransaction();

            $post = new Post();

            $post->name = $request['name'];
            $post->slug = $request['name'];
            $post->description = $request['summary'];
            $post->content = $request['complete_description'];
            $post->orderby = 0;
            $post->status = 1;
            $post->estado = 1;
            $post->rating_number = 0;
            $post->rating_comments = 0;
            $post->likes_number = 0;
            $post->map_lat = $request['center.lat'];
            $post->map_lng = $request['center.lng'];
            $post->map_address = null;//$request['address'];
            $post->user_id = Auth::id();
            $post->feature = 0;
            $post->image = '';
            $post->primary_image = '';
            $post->views = 0;

            $post->save();

            $post_ad = new PostAd();

            $post_ad->post_id = $post->id;
            $post_ad->post_anuncio_transaccion_id = $request['transaction'];
            $post_ad->post_anuncio_tipo_inmueble_id = $request['property_type'];
            $post_ad->geo_department_id = $request['department_id'];
            $post_ad->geo_provinc_id = $request['province_id'];
            $post_ad->geo_distric_id = $request['district_id'];
            $post_ad->geo_address = $request['address'];

            $tipo_cambio = Setting::select('value')->where('key','=','config_tipo_cambio')->get('value')[0]->value;
            $precio_soles = $precio_dolares = 0;
            if ($request['currency'] == '1') {
                $post_ad->ad_money = 1;
                $precio_dolares = $request['price'];
                $precio_soles = $request['price'] * $tipo_cambio;
            }
            if ($request['currency'] == '0') {
                $post_ad->ad_money = 0;
                $precio_soles = $request['price'];
                $precio_dolares = $request['price'] / $tipo_cambio;
            }

            $post_ad->ad_price_pen = $precio_soles;
            $post_ad->ad_price_dolar = $precio_dolares;

            $post_ad->type_group_services = $request['property_type'];
            $post_ad->basic_services = json_encode($request['features.basic']);
            $post_ad->aditional_services = json_encode($request['features.detail']);
            $post_ad->main_services = json_encode($request['features.others']);

            $post_ad->room_services = 0;
            $post_ad->bathroom_services = 0;
            $post_ad->kitchen_services = 0;
            $post_ad->parking_available = 0;
            $post_ad->main_bathroom_services = 0;
            $post_ad->total_area_services = '';
            $post_ad->roofed_area_services = '';
            $post_ad->pool_services = 0;
            $post_ad->furnished_services = 0;
            $post_ad->allow_childrens = 0;
            $post_ad->allow_pets = 0;
            $post_ad->property_age = 0;
            $post_ad->location_description = '';

            // campos desestimados
            $post_ad->near_store = '';
            $post_ad->near_park = '';
            $post_ad->near_clinic = '';
            $post_ad->near_school = '';
            $post_ad->near_library = '';
            $post_ad->near_bank = '';
            $post_ad->near_vet = '';

            $post_ad->principal_city = '';
            $post_ad->type_services = '';
            $post_ad->other_services = '';
            $post_ad->desc_service_1 = '';
            $post_ad->desc_service_2 = '';
            $post_ad->discount_price_1 = '';
            $post_ad->discount_price_2 = '';
            $post_ad->discount_price_3 = '';
            $post_ad->comments = '';

            $post->ads()->save($post_ad);

            $path_from = 'temp'.DIRECTORY_SEPARATOR.$hash_id.DIRECTORY_SEPARATOR;

            $exists = Storage::disk('local')->exists($path_from.$request['primary_image']);

            if($exists)
            {
                $info = pathinfo(public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$path_from.$request['primary_image']);
                $date_str = date('dmYhisu');
                $milliseconds = round(microtime(true) * 1000);


                \Log::info('$info' , [$info]);

                $avatar = [
                    'path'     => 'proyectos/'.$post->id.'/primary_full.' .$date_str . '.' . $milliseconds . '.' . $info['extension'],
                    'realPath' => 'proyectos/'.$post->id.'/primary_thumb.' . $date_str . '.' . $milliseconds . '.' . $info['extension'],
                ];

                Storage::copy($path_from.$request['primary_image'],$avatar['path']);

                $url_image_thumb = $this->resize_img('uploads/' . $avatar['path'], ['save_url' => 'uploads/proyectos/' . $post->id, 'width' => 210, 'height' => 150, 'fit' => true, 'quality' => 90,]);
                $url_image_resize = $this->resize_img('uploads/' . $avatar['path'], ['save_url' => 'uploads/proyectos/' . $post->id, 'width' => 257, 'height' => 338, 'fit' => true, 'resize' => true, 'quality' => 90,]);

                $post->image = $url_image_resize;
                $post->primary_image = 'uploads'.DIRECTORY_SEPARATOR.$avatar['path'];
                $post->save();
            }

            $files = Storage::disk('local')->files($path_from);

            foreach ($files as $filename) {

                $date_str = date('dmYhisu');
                $milliseconds = round(microtime(true) * 1000);
                $info = pathinfo(public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$filename);

                if(!str_contains($info['filename'],'primary_image'))
                {
                    $avatar = [
                        'path'     => 'proyectos/'.$post->id.'/galfull.' . $date_str . '.' . $milliseconds . '.' . $info['extension'],
                        'realPath' => 'proyectos/'.$post->id.'/galthumb.' . $date_str . '.' . $milliseconds . '.' . $info['extension'],
                        'ext'      => $info['extension'],
                        'mime'     => mime_content_type($info['dirname'].DIRECTORY_SEPARATOR.$info['basename']),
                        'name'     => $info['filename'],
                        'user'     => Auth::id(),
                        'size'     => Storage::size($filename)
                    ];

                    $uploaded = Storage::put($avatar['path'], file_get_contents($info['dirname'].DIRECTORY_SEPARATOR.$info['basename']), 'public');
                    $url_image_thumb = $this->resize_img('uploads/' . $avatar['path'], ['save_url' => 'uploads/proyectos/' . $post->id, 'width' => 210, 'height' => 150, 'fit' => true, 'quality' => 90,]);
                    $url_image_resize = $this->resize_img('uploads/' . $avatar['path'], ['save_url' => 'uploads/proyectos/' . $post->id, 'width' => 257, 'height' => 338, 'fit' => true, 'resize' => true, 'quality' => 90,]);

                    $obj_post_anuncio_galeria = new PostAdGallery();
                    $obj_post_anuncio_galeria->image = 'uploads/' . $avatar['path'];
                    $obj_post_anuncio_galeria->image_thumb = $url_image_thumb;
                    $obj_post_anuncio_galeria->image_resize = $url_image_resize;
                    $obj_post_anuncio_galeria->type = 1;
                    $obj_post_anuncio_galeria->order = 9999999;
                    $post->galleries()->save($obj_post_anuncio_galeria);
                }

            }

            \DB::commit();

            return [
                'success' => true,
                'message' =>  'Anuncio publicado correctamente',
            ];

        } catch (\Throwable $e) {
            \Log::error($e);
            \DB::rollback();

            return [
                'success' => false,
                'message' =>  'Existen errores en la transacción',
            ];

        }
    }

    public function setFechas(Request $request)
    {
        \Log::info($request);

        try {

            $post_ad_calendar = new PostAdCalendar();

            $post_ad_calendar->post_id = $request['idAds'];
            $post_ad_calendar->title = '';
            $post_ad_calendar->type = 1;
            $post_ad_calendar->startdate = $request['fechaIni'];
            $post_ad_calendar->enddate = $request['fechaFin'];
            $post_ad_calendar->allDay = 1;

            $post_ad_calendar->save();

            return [
                'success' => true,
                'message' =>  'Fechas asignadas correctamente.',
            ];

        } catch (\Throwable $e) {
            \Log::error($e);

            return [
                'success' => false,
                'message' =>  'Existen errores en la transacción',
            ];

        }
    }

    public function getFechas($post)
    {
        return [
            'fechas' => PostAdCalendar::select('post_id', 'startdate', 'enddate')->where('post_id', '=', "{$post}")->limit(10)->get()
        ];
    }
}
