<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostCollection;
use App\Http\Resources\PostFavoriteUserCollection;
use App\Models\Post;
use App\Models\PostFavoriteUser;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');

        $currentUser = User::findOrFail(Auth::id());

        return view('profile.edit', compact('currentUser',$currentUser));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function favorite()
    {
        return view('profile.favorite')->with('id',Auth::id());
    }

    /**
     * Get favorities ads to relate with Id User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getFavorities($id)
    {
        $records = Post::whereHas('postfavoriteuser', function (Builder $query) use($id) {
            $query->where('user_id','=', $id );
        })->get();
        return new PostCollection($records);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ourAds()
    {
        return view('profile.ourads')->with('id',Auth::id());
    }

    /**
     * Get ads that was created by User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getOurAds($id)
    {
        $records = Post::where("user_id","=",$id)->get();
        return new PostCollection($records);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $id = Auth::id();
        $row = User::firstOrNew(['id' => $id]);
        $row->fill($request->all());
        $row->save();

        return redirect()->route('profile.edit')->with('success','Registro editado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
