<?php

namespace App\Http\Traits;
use Image;

trait UploadImageTrait {

    function base_convert_arbitrary($number, $fromBase, $toBase)
    {
        $digits = '0123456789abcdefghijklmnopqrstuvwxyz';
        $length = strlen($number);
        $result = '';

        $nibbles = array();
        for ($i = 0; $i < $length; ++$i) {
            $nibbles[$i] = strpos($digits, $number[$i]);
        }

        do {
            $value = 0;
            $newlen = 0;

            for ($i = 0; $i < $length; ++$i) {

                $value = $value * $fromBase + $nibbles[$i];

                if ($value >= $toBase) {
                    $nibbles[$newlen++] = (int)($value / $toBase);
                    $value %= $toBase;

                } else if ($newlen > 0) {
                    $nibbles[$newlen++] = 0;
                }
            }

            $length = $newlen;
            $result = $digits[$value] . $result;
        } while ($newlen != 0);

        return $result;
    }

    function resize_img($url, $args = array())
    {
        $url_image = $url;

        $defaults = array(
            'save_url'           => null,
            'width'              => false,
            'height'             => false,
            'crop'               => FALSE,
            'fit'                => FALSE,
            'resize'             => FALSE,
            'resizeCanvas'       => FALSE,
            'watermark'          => FALSE,
            'watermark_position' => FALSE,
            'watermark_tipo'     => FALSE,
            'quality'            => 75,
        );
        $options = array_merge($defaults, $args);
        extract($options, EXTR_PREFIX_SAME, "tr");

        $suffix = (string)filemtime($url_image) . (isset($width) ? str_pad((string)$width, 5, '0', STR_PAD_LEFT) : '00000') . (isset($height) ? str_pad((string)$height, 5, '0', STR_PAD_LEFT) : '00000') . (isset($opacity) ? str_pad((string)$opacity, 3, '0', STR_PAD_LEFT) : '100') . (isset($color) ? str_pad(preg_replace('#^\##', '', $color), 8, '0', STR_PAD_LEFT) : '00000000') . (isset($grayscale) ? ($grayscale ? '1' : '0') : '0') . (isset($crop) ? ($crop ? '1' : '0') : '0') . (isset($fit) ? ($fit ? '1' : '0') : '0') . (isset($resize) ? ($resize ? '1' : '0') : '0') . (isset($watermark) ? ($watermark ? '1' : '0') : '0') . (isset($watermark_position) ? str_pad((string)$watermark_position, 5, '0', STR_PAD_LEFT) : '00000') . (isset($watermark_tipo) ? str_pad((string)$watermark_tipo, 5, '0', STR_PAD_LEFT) : '00000') . (isset($negate) ? ($negate ? '1' : '0') : '0') . (isset($crop_only) ? ($crop_only ? '1' : '0') : '0') . (isset($src_x) ? str_pad((string)$src_x, 5, '0', STR_PAD_LEFT) : '00000') . (isset($src_y) ? str_pad((string)$src_y, 5, '0', STR_PAD_LEFT) : '00000') . (isset($src_w) ? str_pad((string)$src_w, 5, '0', STR_PAD_LEFT) : '00000') . (isset($src_h) ? str_pad((string)$src_h, 5, '0', STR_PAD_LEFT) : '00000') . (isset($dst_w) ? str_pad((string)$dst_w, 5, '0', STR_PAD_LEFT) : '00000') . (isset($dst_h) ? str_pad((string)$dst_h, 5, '0', STR_PAD_LEFT) : '00000') . ((isset ($quality) && $quality > 0 && $quality <= 100) ? ($quality ? (string)$quality : '0') : '0');
        $suffix = $this->base_convert_arbitrary($suffix, 10, 36);

        $img = Image::make($url_image);
        if ($crop == true) {
            $img->crop($width, $height);
        }
        if ($fit == true) {
            $img->fit($width, $height);
        }
        if ($resize == true) {
            $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }
        if ($resizeCanvas == true) {
            $img->resizeCanvas($width, $height, 'center', false, 'ffffff');
        }


        $dir_save = 'uploads/' . 'resize_crop';
        if ($save_url && !empty($save_url)) {
            $dir_save = $save_url;
        }

        $url_save = $dir_save . '/' . pathinfo($url, PATHINFO_FILENAME) . '-' . $suffix . '.' . pathinfo($url, PATHINFO_EXTENSION);//str_replace(pathinfo($url, PATHINFO_FILENAME), , $url);
        $img->save($url_save, $quality);
        return $url_save;
    }

}
