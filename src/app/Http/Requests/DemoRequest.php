<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DemoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'clinic_history_id' => 'required',
            'medical_dx_physio_id' => 'required',
            'forecast' => 'required',
            'structure_bio' => 'required',
            'doctor' => 'required',
            'recommended_sessions' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'medical_dx_physio_id' => 'diagnóstico fisioterapeutico',
            'forecast' => 'pronóstico',
            'structure_bio' => 'estructural/biomecánico',
            'doctor' => 'licenciado',
            'recommended_sessions' => 'sesiones recomendadas'
        ];
    }
}

