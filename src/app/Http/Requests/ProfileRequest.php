<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'email' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'firstname' => 'first name',
            'email' => 'email'
        ];
    }
}

