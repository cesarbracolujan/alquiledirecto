<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'name' => 'required',
            'description' => 'required',
            'content' => 'required',
            'map_lat' => 'required',
            'map_lng' => 'required',
            'slug' => 'required',
            'property_type_id' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'título',
            'description' => 'descripción',
            'content' => 'contenido',
            'slug' => 'slug',
            'property_type_id' => 'tipo inmueble'
        ];
    }
}

