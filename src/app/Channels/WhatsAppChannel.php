<?php

namespace App\Channels;

use App\Mail\QuestionEmail;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;

class WhatsAppChannel
{
    public static function send($from, $to, $message)
    {
        $from = config('services.twilio.whatsapp_from');

        $twilio = new Client(config('services.twilio.sid'), config('services.twilio.token'));

        return $twilio->messages->create(
            "whatsapp:".$to,
            array(
                "from" => "whatsapp:".$from,
                "body" => $message
            )
        );
    }

    public static function sentEmail($email, $objQuestion)
    {
        Mail::to($email)->send(new QuestionEmail($objQuestion));
    }
}
