<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class PostAdTransactionsImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('post_ad_transactions')->insert([
                [
                    'name' => $row[0],
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null,
                    'deleted_at'=> null]
            ]);
        }
    }
}
