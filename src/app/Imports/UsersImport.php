<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class UsersImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('users')->insert([
                [
                    'email' => $row[0],
                    'email_verified_at' => date('Y-m-d H:i:s'),
                    'password' => $row[2],
                    'type' => $row[4],
                    'firstname' => $row[5],
                    'lastname' => $row[6],
                    'contact_name' => $row[7],
                    'address' => $row[8],
                    'phone' => $row[9],
                    'gender' => $row[10],
                    'profile_image' => $row[11],
                    'remember_type' => $row[12],
                    'website' => $row[13],
                    'facebook' => $row[14],
                    'twitter' => $row[15],
                    'youtube' => $row[16],
                    'completed_profile' => true,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null,
                    'deleted_at'=> null]
            ]);
        }
    }
}
