<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class PostAdGalleriesImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('post_ad_galleries')->insert([
                [
                    'post_id' => $row[0],
                    'image' => $row[1],
                    'image_thumb' => $row[2],
                    'image_resize' => $row[3],
                    'type' => $row[4],
                    'order' => $row[5],
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null]
            ]);
        }
    }
}
