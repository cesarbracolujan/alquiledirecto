<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class PostFeaturesImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('post_features')->insert([
                [
                    'order' => $row[0],
                    'post_id' => $row[1],
                    'post_ad_feature_id' => $row[2],
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null]
            ]);
        }
    }
}
