<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class PostsImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('posts')->insert([
                [
                    'name' => $row[0],
                    'slug' => $row[1],
                    'description' => $row[2],
                    'content' => $row[3],
                    'orderby' => $row[4],
                    'status' => $row[5],
                    'estado' => $row[6],
                    'rating_number' => $row[7],
                    'rating_comments' => $row[8],
                    'likes_number' => $row[9],
                    'map_lat' => $row[10],
                    'map_lng' => $row[11],
                    'map_address' => $row[12],
                    'user_id' => $row[13],
                    'feature' => $row[14],
                    'image' => $row[15],
                    'primary_image' => $row[16],
                    'views' => $row[17],
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null,
                    'deleted_at'=> null]
            ]);
        }
    }
}
