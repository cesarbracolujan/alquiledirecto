<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class PostAdsImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('post_ads')->insert([
                [
                    'post_id' => $row[0],
                    'post_anuncio_transaccion_id' => $row[1],
                    'post_anuncio_tipo_inmueble_id' => $row[2],
                    'geo_department_id' => $row[3],
                    'geo_provinc_id' => $row[4],
                    'geo_distric_id' => $row[5],
                    'geo_address' => $row[6],
                    'ad_money' => $row[7],
                    'ad_price_pen' => $row[8],
                    'ad_price_dolar' => $row[9],
                    'type_group_services' => $row[10],
                    'basic_services' => $row[11],
                    'aditional_services' => $row[12],
                    'main_services' => $row[13],
                    'room_services' => $row[14],
                    'bathroom_services' => $row[15],
                    'kitchen_services' => $row[16],
                    'parking_available' => $row[17],
                    'main_bathroom_services' => $row[18],
                    'total_area_services' => $row[19],
                    'roofed_area_services' => $row[20],
                    'pool_services' => $row[21],
                    'furnished_services' => $row[22],
                    'allow_childrens' => $row[23],
                    'allow_pets' => $row[24],
                    'property_age' => $row[25],
                    'location_description' => $row[26],
                    'near_store' => $row[27],
                    'near_park' => $row[28],
                    'near_clinic' => $row[29],
                    'near_school' => $row[30],
                    'near_library' => $row[31],
                    'near_bank' => $row[32],
                    'near_vet' => $row[33],
                    'principal_city' => $row[34],
                    'type_services' => $row[35],
                    'other_services' => $row[36],
                    'desc_service_1' => $row[37],
                    'desc_service_2' => $row[38],
                    'discount_price_1' => $row[39],
                    'discount_price_2' => $row[40],
                    'discount_price_3' => $row[41],
                    'comments' => $row[42],
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null,
                    'deleted_at'=> null]
            ]);
        }
    }
}
