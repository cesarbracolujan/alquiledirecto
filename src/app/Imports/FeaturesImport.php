<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class FeaturesImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('features')->insert([
                [
                    'name' => $row[0],
                    'html_content' => $row[1],
                    'feature_type' => $row[2],
                    'feature_icon' => $row[3],
                    'input_type' => $row[4],
                    'has_value' => $row[5],
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null,
                    'deleted_at'=> null]
            ]);
        }
    }
}
