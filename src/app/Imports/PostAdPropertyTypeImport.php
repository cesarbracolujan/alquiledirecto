<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class PostAdPropertyTypeImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            DB::table('post_ad_property_types')->insert([
                [
                    'name' => $row[0],
                    'icon_image' => $row[1],
                    'type_data' => $row[2],
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> null,
                    'deleted_at'=> null]
            ]);
        }
    }
}
