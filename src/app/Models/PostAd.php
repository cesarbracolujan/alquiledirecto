<?php

namespace App\Models;

class PostAd extends BaseModel
{
    protected $fillable = [
        'post_id',
        'post_anuncio_transaccion_id',
        'post_anuncio_tipo_inmueble_id',
        'geo_department_id',
        'geo_department_id',
        'geo_provinc_id',
        'geo_distric_id',
        'geo_address',
        'ad_price_pen',
        'ad_price_dolar',
        'ad_money'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function propertyType()
    {
        return $this->belongsTo(CatPostAdPropertyType::class, 'post_anuncio_tipo_inmueble_id','id');
    }

    public function transaction()
    {
        return $this->belongsTo(CatPostAdTransaction::class, 'post_anuncio_transaccion_id','id');
    }

}
