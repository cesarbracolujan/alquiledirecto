<?php

namespace App\Models;

class PostFavoriteUser extends BaseModel
{
    protected $table = 'post_favorite_users';

    protected $fillable = [
        'order',
        'post_id',
        'user_id'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class,'id','post_id');
    }

}
