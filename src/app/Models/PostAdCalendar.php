<?php

namespace App\Models;

class PostAdCalendar extends BaseModel
{
    protected $fillable = [
        'post_id',
        'title',
        'type',
        'startdate',
        'enddate',
        'allDay'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
