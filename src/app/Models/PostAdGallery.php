<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class PostAdGallery extends Model
{
    protected $fillable = [
        'post_id',
        'image',
        'image_thumb',
        'image_resize',
        'type',
        'order',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
