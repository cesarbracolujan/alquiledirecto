<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class GeoDistrict extends Model
{
    public function getSelectedAttribute()
    {
        return false;
    }

    protected $appends = ['selected'];
}
