<?php

namespace App\Models;

class CatPostAdPropertyType extends BaseModel
{
    protected $table = 'post_ad_property_types';

    protected $fillable = [
        'name',
        'icon_image',
        'type_data',
    ];

    public function getSelectedAttribute()
    {
        return false;
    }

    protected $appends = ['selected'];
}
