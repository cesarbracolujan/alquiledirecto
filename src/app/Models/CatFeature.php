<?php

namespace App\Models;

class CatFeature extends BaseModel
{
    protected $table = 'features';

    protected $fillable = [
        'name',
        'html_content',
        'feature_type',
        'input_type',
        'feature_icon',
        'has_value'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
   ];
}
