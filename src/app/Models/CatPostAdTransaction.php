<?php

namespace App\Models;

class CatPostAdTransaction extends BaseModel
{
    protected $table = 'post_ad_transactions';

    protected $fillable = [
        'name',
    ];

    public function getSelectedAttribute()
    {
        return false;
    }

    protected $appends = ['selected'];
}
