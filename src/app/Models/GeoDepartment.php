<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class GeoDepartment extends Model
{
    public function getSelectedAttribute()
    {
        return false;
    }

    protected $appends = ['selected'];
}
