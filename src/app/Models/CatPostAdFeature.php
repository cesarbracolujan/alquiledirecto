<?php

namespace App\Models;

class CatPostAdFeature extends BaseModel
{
    protected $table = 'post_ad_features';

    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
   ];

    public function getSelectedAttribute()
    {
        return false;
    }

    protected $appends = ['selected'];
}
