<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PostFeature extends Model
{
    protected $fillable = [
        'order',
        'post_id',
        'post_ad_feature_id',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
