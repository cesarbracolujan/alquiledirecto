<?php

namespace App\Models;

use App\User;
use GuzzleHttp\RetryMiddleware;

class Post extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'description',
        'slug',
        'content',
        'orderby',
        'image'
    ];

    public function ads()
    {
        return $this->hasOne(PostAd::class);
    }

    public function features()
    {
        return $this->hasMany(PostFeature::class);
    }

    public function galleries()
    {
        return $this->hasMany(PostAdGallery::class);
    }

    public function postfavoriteuser()
    {
        return $this->hasMany(PostFavoriteUser::class,'post_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

}
