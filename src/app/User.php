<?php

namespace App;

use App\Notifications\VerifyEmailNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'password',
        'email',
        'type',
        'has_whatsapp',
        'term_accepted',
        'contact_name',
        'address',
        'phone',
        'gender',
        'profile_image',
        'remember_type',
        'website',
        'facebook',
        'twitter',
        'youtube',
        'token',
        'completed_profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function socialProfiles() {
        return $this->hasMany(SocialProfile::class);
    }

    public function avatar() {
        $social_profile = $this->socialProfiles->first();
        if($social_profile) {
            return $social_profile->social_avatar;
        }
        else {
            return '';
        }
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification());
    }
}
